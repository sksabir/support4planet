<?php
//Add comment for event
if ($_POST['comment'] != '') {
    $user_ID = get_current_user_id();
    $user = get_userdata($user_ID);
    $fName = get_user_meta($user_ID, 'first_name', true);
    $lName = get_user_meta($user_ID, 'last_name', true);

    $commentdata = array(
        'comment_post_ID' => get_the_ID(), // to which post the comment will show up
        'comment_author' => $fName . ' ' . $lName, //fixed value - can be dynamic 
        'comment_author_email' => $user->user_email, //fixed value - can be dynamic 
        'comment_author_url' => $user->user_url, //fixed value - can be dynamic 
        'comment_content' => $_POST['comment'], //fixed value - can be dynamic 
        'comment_type' => '', //empty for regular comments, 'pingback' for pingbacks, 'trackback' for trackbacks
        'comment_parent' => 0, //0 if it's not a reply to another comment; if it's a reply, mention the parent comment ID here
        'user_id' => $user_ID, //passing current user ID or any predefined as per the demand
    );

    //Insert new comment and get the comment ID
    $comment_id = wp_new_comment($commentdata);
}
get_header();
wp_reset_query();

global $wpdb;
global $current_user;
//$user_login=$current_user->user_login;

if ($current_user) {
    $login_user_id = $current_user->ID;
}
$single_cause_page_link = get_permalink(get_the_ID()); // single cause page link
?>
<div class="container">
    <div class="row"> 
        <!-- First title -->
        <div class="donor-comment-section">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <h2><?php the_title(); ?></h2>
            </div>
            <div class="col-sm-12 col-md-8"> 
                <!-- Like Banner -->
                <?php
                $feat_image = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
                if ($feat_image == '') {
                    $feat_image = get_template_directory_uri() . "/images/single_cause_noimage.jpg";
                }
                ?>
                <div class="donar-banner"><img alt="" src="<?php echo $feat_image; ?>">
                    <?php
                    $no_of_cause_like = '';
                    $cause_post_like = $wpdb->get_results("SELECT id FROM {$wpdb->prefix}single_post_rating WHERE `post_id` = '" . get_the_ID() . "'");
                    $no_of_cause_like = count($cause_post_like);


                    if (is_user_logged_in()) {

                        echo '<a class="rate_post" href="javascript:void(0);"><span><p>' . $no_of_cause_like . '</p></span></a>';
                    } else {
                        echo '<span><p>' . $no_of_cause_like . '</p></span>';
                    }
                    ?>

                </div>
                <!-- Like Banner End --> 
                <!-- Charitable -->
                <div class="col-sm-6 col-md-6">
                    <?php
                    global $post;
                    $author_id = $post->post_author;
//$name = get_the_author_meta( 'first_name' , $author_id ). ' ' .get_the_author_meta( 'last_name' , $author_id );
                    $pic = get_field('profile_picture', 'user_' . $author_id);
                    if (!empty($pic)) {
                        $profile_picture = get_field('profile_picture', 'user_' . $author_id);
                        $url = $profile_picture['url'];
                        $alt = $profile_picture['alt'];
                    } else {
                        $url = get_template_directory_uri() . '/images/blank.jpg';
                        $alt = '';
                    }
                    ?>
                    <div class="charitable"> <span style="background: #f6f6f6 url('<?php echo $url; ?>') no-repeat scroll 0 0 / cover" class="curcal-gry"></span>
                        <div class="chari-right">
                            <h2>A Charitable Fundraiser By</h2>
                            <?php $name = get_field('organization_name'); ?>
                            <p><?php echo $name; ?></p>
                            <?php $state_category = get_the_terms(get_the_ID(), 'state_category'); ?>
                            <h3><?php echo get_field('city'); ?>, <?php echo $state_category[0]->name; ?></h3>
                        </div>
                    </div>
                </div>
                <!-- Charitable End --> 
                <!-- Tag Line -->
                <div class="col-sm-7 col-lg-7 tag-line">
                    <p><strong>Tags: </strong><?php echo get_field('enter_tags'); ?></p>
                </div>
                <!-- Tag Line End --> 

                <!-- Tab Section -->
                <div class="col-sm-12 col-md-12 tab-content">
                    <div id="parentHorizontalTab" style="display: block; width: 100%; margin: 0px;">
                        <ul class="resp-tabs-list hor_1">
                            <li class="active resp-tab-item hor_1 resp-tab-active" aria-controls="hor_1_tab_item-0" role="tab" style="background-color: white; border-color: rgb(193, 193, 193);">The Event </li>
                            <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-1" role="tab" style="background-color: white; border-color: rgb(193, 193, 193);">Latest Updates </li>
                            <li class="resp-tab-item hor_1" aria-controls="hor_1_tab_item-2" role="tab" style="background-color: white; border-color: rgb(193, 193, 193);">Comments</li>
                        </ul>
                        <div class="resp-tabs-container hor_1" style="border-color: rgb(193, 193, 193);">
                            <!-- Community Three -->
                            <h2 role="tab" class="resp-accordion hor_1 resp-tab-active" style="border-color: rgb(193, 193, 193); background-color: white;" aria-controls="hor_1_tab_item-0"><span class="resp-arrow"></span>The Event </h2>
                            <div class="community-three resp-tab-content hor_1 resp-tab-content-active" aria-labelledby="hor_1_tab_item-0" style="display:block">
                                <?php the_content(); ?>  
                            </div>
                            <!-- Community Three -->
                            <?php
                            $updates = get_field('updates');
//echo '<pre>';
//print_r($updates);
//echo '</pre>';
                            ?>
                            <!-- Community Two -->
                            <h2 role="tab" class="resp-accordion hor_1" style="border-color: rgb(193, 193, 193); background-color: white;" aria-controls="hor_1_tab_item-1"><span class="resp-arrow"></span>Latest Updates </h2>
                            <div class="community-two resp-tab-content hor_1<?php
                            if (empty($updates)) {
                                echo ' no-background';
                            }
                            ?>" aria-labelledby="hor_1_tab_item-1" >

                                <?php
                                if (!empty($updates)) {
                                    ?>
                                    <ul>
    <?php foreach ($updates as $update) { ?>
                                            <li>
                                                <span class="valu-community"></span>
                                                <div class="community-inner">
                                                    <h3><?php echo date('jS M, Y', strtotime($update['date'])); ?></h3>
                                                    <h2><a><?php echo $update['title']; ?></a></h2>
                                                    <p><?php echo $update['content']; ?></p>
                                                </div>
                                            </li>
                                    <?php } ?>
                                    </ul>
                                <?php
                                } else {
                                    echo '<p style="color:#7d8b9c;">Currently no update is here!!!</p>';
                                }
                                ?>
                            </div>
                            <!-- Community Two End -->

                            <!-- Tab Comments -->
                            <h2 role="tab" class="resp-accordion hor_1" style="border-color: rgb(193, 193, 193); background: white none repeat scroll 0% 0%;" aria-controls="hor_1_tab_item-2"><span class="resp-arrow"></span>Comments</h2>
                            <div class="tab-comments resp-tab-content hor_1" aria-labelledby="hor_1_tab_item-2">
                                <?php
                                if (is_user_logged_in()) {
                                    $user_ID = get_current_user_id();
                                    $user = get_userdata($user_ID);
                                    $roles = implode(', ', $user->roles);
                                    //print_r($user);
                                    if ($roles == 'individual') {
                                        ?>
                                        <form class="form-horizontal" method="post" action="">
                                            <fieldset>

                                                <!-- Form Name --> 

                                                <!-- Textarea -->
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <textarea name="comment" required="" id="textarea" class="form-control">Join the conversation</textarea>
                                                    </div>
                                                </div>

                                                <!-- Button -->
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn btn-primary" name="singlebutton" id="singlebutton">Leave A Comment ( for donors only)</button>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    <?php
                                    }
                                }
                                ?>
                                <?php
                                $args = array(
                                    'status' => 'approve',
                                    'post_id' => get_the_ID(), // use post_id, not post_ID
                                );
                                $comments = get_comments($args);
                                //print_r($comments);
                                if (!empty($comments)) {
                                    foreach ($comments as $comment) {
                                        $u_ID = $comment->user_id;
                                        $pic = get_field('profile_picture', 'user_' . $u_ID);
                                        if (!empty($pic)) {
                                            $profile_picture = get_field('profile_picture', 'user_' . $u_ID);
                                            $url = $profile_picture['url'];
                                            $alt = $profile_picture['alt'];
                                        } else {
                                            $url = get_template_directory_uri() . '/images/blank.jpg';
                                            $alt = '';
                                        }
                                        ?>
                                        <div class="media">
                                            <div class="media-left"> <a> <img width="80" height="80" alt="<?php echo $alt; ?>" class="img-circle" src="<?php echo $url; ?>"> </a> </div>
                                            <div class="media-body">
                                                <h4 class="media-heading"><?php echo $comment->comment_author; ?></h4>
                                                <em><?php printf(_x('%s ago', '%s = human-readable time difference', 'your-text-domain'), human_time_diff(strtotime($comment->comment_date))); ?>
                                        <?php //echo date('jS M, Y',strtotime($comment->comment_date));  ?></em>
                                                <p><?php echo $comment->comment_content; ?></p>
                                            </div>
                                        </div>
    <?php
    }
} else {
    echo '<p style="color:#7d8b9c;font-size: 14px;line-height: 25px;margin: 0 0 10px;">Currently no comment is here!!!</p>';
}
?>
                            </div>
                            <!-- Tab Comments End -->
                        </div>
                    </div>
                </div>
                <!-- Tab Section End --> 
            </div>
            <!-- Right Species Section -->
            <div class="col-sm-12 col-md-4"> 
                <!-- Species Section -->
                <div class="species-section">
                    <ul>

                        <li><span class="water-one"></span>
                            <div class="pirce-d">
                                <?php
                                $amount_qry = "SELECT SUM(`amount`) FROM {$wpdb->prefix}donation WHERE `post_id`='" . get_the_ID() . "' AND `status`='paid'";
                                $amount_query_run = $wpdb->get_results($amount_qry, OBJECT);
                                $amount_res = "SUM(`amount`)";
                                $gain_fund = $amount_query_run[0]->$amount_res;

                                $required_fund = get_field('fundraising_goal_event');
                                //echo '<pre>';
                                //print_r($query_run[0]->$res);
                                //echo '</pre>';
                                ?>
                                <h2>$<?php echo number_format($gain_fund); ?></h2>
                                <p>Pleadged of $<?php echo number_format($required_fund); ?> goal</p>
                            </div>
                        </li>
                        <li><span class="water-two"></span>
                            <div class="pirce-d">
                                <?php
                                $doner_qry = "SELECT COUNT(DISTINCT `user_id`) FROM {$wpdb->prefix}donation WHERE `post_id`='" . get_the_ID() . "' AND `status`='paid'";
                                $doner_query_run = $wpdb->get_results($doner_qry, OBJECT);
                                $doner_res = "COUNT(DISTINCT `user_id`)";
                                //echo '<pre>';
                                //print_r($query_run[0]->$res);
                                //echo '</pre>';
                                ?>
                                <h2><?php echo $doner_query_run[0]->$doner_res; ?></h2>
                                <p>No. of donors</p>
                            </div>
                        </li>
                        <li><span class="water-three"></span>
                            <div class="pirce-d">
                                <?php
                                $end_date = get_field('end_date_time', $post->ID);
                                $now = time(); // or your date as well
                                $end_date = strtotime($end_date);
                                $datediff = floor(($end_date - $now) / (60 * 60 * 24));
                                if ($datediff >= 0) {
                                    $left = '<h2>' . $datediff . '</h2><p>days left</p>';
                                } else {
                                    $left = '<h2>Completed!!!</h2>';
                                }
                                ?>
                <?php echo $left; ?>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- Species Section End --> 
                        <?php
                        $behavior_of_event = get_field('behavior_of_event');
                        //print_r($behavior_of_event);
                        if (in_array('Fundraiser', $behavior_of_event)) {
                            ?>
                    <!-- Donate Search -->
                    <div class="donate-search">
                        <div class="row">
    <?php
    if ($gain_fund >= $required_fund) {
        echo '<div style="text-align: center;" class="col-sm-12 col-md-12 col-xs-12"><span style="color:#0c0;">Fund Fulfilled!!!</span></div>';
    } else {
        ?>	
                                <div class="col-sm-12 col-md-12 col-xs-12"> 
                                    <div class="input-group">
                                        <input type="text" id="enter-amount" class="form-control input-search" pattern="[0-9]+\.[0-9]{,2}$" placeholder="Enter Amount">
                                        <span class="input-group-btn">
                                            <button id="donate-button" data-target=".bs-example-modal-sm" data-toggle="modal" type="button" class="btn btn-primary">
                                                Donate Now</button>
                                        </span> </div>
                                </div>
                    <?php } ?>
                        </div>
                    </div>
                        <?php } ?>
                <!-- Donate Search End --> 
                        <?php if (is_user_logged_in()) { ?>
                            <?php
                            if (in_array('Volunteer', $behavior_of_event)) {
                                ?>
                        <div class="col-sm-12 col-md-12 contact-donat no-padding">
                            <div class="form-group">
                                <?php
                                $volu_qry = "SELECT COUNT(`user_id`) FROM {$wpdb->prefix}volunteer WHERE `post_id`='" . get_the_ID() . "'";
                                $volu_qry_run = $wpdb->get_results($volu_qry, OBJECT);
                                $vol_res = "COUNT(`user_id`)";
                                $volu_found = $volu_qry_run[0]->$vol_res;

                                $required_volu = get_field('no_volunteers');
                                ?>
                                <?php
                                if ($volu_found >= $required_volu) {
                                    $is_volunteer_qry = "SELECT COUNT(`post_id`) FROM {$wpdb->prefix}volunteer WHERE `user_id`='" . $login_user_id . "'";
                                    $query_run = $wpdb->get_results($is_volunteer_qry, OBJECT);
                                    $res = "COUNT(`post_id`)";
                                    $count = $query_run[0]->$res;
                                    //echo '<pre>';
                                    //print_r($query_run[0]->$res);
                                    //echo '</pre>';
                                    if ($count > 0) {
                                        echo '<span style="color:#0c0;">You are already registered as volunteer</span>';
                                    } else {
                                        echo '<div style="text-align: center;" class="col-sm-12 col-md-12 col-xs-12"><span style="color:#0c0;">Volunteers Fulfilled!!!</span></div>';
                                    }
                                } else {
                                    ?>	

                                    <div class="add_volunteer" class="col-sm-12 col-md-12 no-padding">
                                        <?php
                                        $is_volunteer_qry = "SELECT COUNT(`post_id`) FROM {$wpdb->prefix}volunteer WHERE `user_id`='" . $login_user_id . "'";
                                        $query_run = $wpdb->get_results($is_volunteer_qry, OBJECT);
                                        $res = "COUNT(`post_id`)";
                                        $count = $query_run[0]->$res;
                                        //echo '<pre>';
                                        //print_r($query_run[0]->$res);
                                        //echo '</pre>';
                                        if ($count > 0) {
                                            echo '<span style="color:#0c0;">You are already registered as volunteer</span>';
                                        } else {
                                            echo '<button style="padding: 14px 145px;" class="btn btn-primary" name="singlebutton" id="join_as_volunteer">Join as Volunteer</button>';
                                        }
                                        ?>

                                    </div>
        <?php } ?>
                            </div>
                        </div>
    <?php } ?>
<?php } ?>
                <!-- Share Point Box -->

                <div class="social-share-box">
                    <div class="col-sm-6 col-md-12 no-padding ">
                        <h2>Share The Cause</h2>
                        <div class="col-sm-12 col-md-12 no-padding share-point">
                            <div id="tw-share" class="col-sm-12 col-md-3 social-share-facebook"></div>
                            <div id="fb-share" class="col-sm-12 col-md-3 social-share-facebook"></div>
                            <div id="li-share" class="col-sm-12 col-md-3 social-share-facebook"></div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-12 no-padding ">
                        <h2>Share Via Mail</h2>
                        <div class="col-sm-12 col-md-12 no-padding">
                            <form>
                                <div class="row">
                                    <div id="share_link_box" class="col-sm-12 col-md-12 col-xs-12">
                                        <div class="input-group">
                                            <input id="share_email" type="text" class="form-control input-search" placeholder="Enter Email of your friend">
                                            <span class="input-group-btn">
                                                <button id="send_email" type="button" data-url="<?php echo site_url(); ?>/ajax/share_by_email.php" data-id="<?php echo get_the_ID(); ?>" class="btn btn-primary">Send Mail</button>
                                            </span> </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
                <!-- Share Point Box End --> 

                <!-- Contact Donat Section-->

                <div class="col-sm-12 col-md-12 contact-donat no-padding">
                    <h2>Do more for the cause</h2>
                    <!-- Button -->
                    <div class="form-group">
                        <div class="col-sm-12 col-md-12 no-padding">
                            <button data-target=".contact-cause-creator" data-toggle="modal" class="btn btn-primary" name="singlebutton" id="singlebutton">Contact Cause Creator</button>
                        </div>
                    </div>
                </div>
                <!-- Contact Donat Section End --> 
            </div>
            <!-- Right Species Section End --> 
        </div>

        <!-- First title End -->



    </div>
</div>
<div aria-labelledby="mySmallModalLabel" role="dialog" tabindex="-1" class="modal fade bs-example-modal-sm">
    <div class="modal-dialog donateModal">
        <div class="modal-content outLine">
            <!--<form class="innerBox form-horizontal formStep-1">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"></span>
            </button>
                <h2>Make a Donation</h2>
               
                <div class="form-group">
                    <div class="col-xs-4"><img src="images/img115X87.jpg" alt=""></div>
                    <div class="col-xs-8">
                        <h4>Throw Me a Bone, Mista!</h4>
                        <p>Supporting charities our community believes in by helping them reach and</p>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="col-xs-5"><h4>Enter the amount:</h4></div>
                    <div class="col-xs-5">
                        <input type="text" class="form-control" placeholder="Amount"> 
                    </div>
                    <div class="col-xs-2"><span>USD</span></div>
                </div>
             
                 <footer class="formFoot">
                       <a href="#" class="goBack">Go Back</a>
                       <a href="#" class="yellowBtn">Enter Billing Details</a>
                       
                </footer>
                
            </form>-->
            <form id="paypal-form" method="post" action="<?php echo site_url() ?>/ajax/donation.php" class="innerBox form-horizontal formStep-2">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="true"></span>
                </button>
                <h2>Billing Details</h2>
                <div class="form-group">
                    <label class="col-xs-5">Name:</label>
                    <div class="col-xs-7">
                        <input type="text" name="first_name" placeholder="First Name" class="first_name form-control" value=""> 
                        <input type="text" name="last_name" placeholder="Last Name" class="last_name form-control" value=""> 
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-xs-5">Postal Address:</label>
                    <div class="col-xs-7">
                        <input type="text" name="address_1" placeholder="Enter the Street address" class="address_1 form-control">
                        <input type="text" value="" name="city" placeholder="Enter the name of the City" class="city form-control">

                        <div class="selectBox">
                            <select class="state form-control selectpicker" name="state" tabindex="-98">
                                <option value="">Select state</option>
                                <?php
                                $taxonomies = get_terms('state_category', array(
                                    'orderby' => 'count',
                                    'hide_empty' => 0,
                                ));
                                if ($taxonomies) {
                                    foreach ($taxonomies as $taxonomy) {
                                        echo '<option value="' . $taxonomy->slug . '">' . $taxonomy->name . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <input value="" type="text" name="zip" placeholder="Zipcode" class="zip form-control">
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-xs-5">Additional Info:</label>
                    <div class="col-xs-7">
                        <input type="text" name="night_phone_a" placeholder="Enter Phone No." class="night_phone_a form-control" value="">
                        <input type="text" name="email" placeholder="Enter Email Id" class="email form-control" value="">
                    </div>
                </div>
                <footer class="formFoot">
                    <input type="hidden" name="post_id" value="<?php echo get_the_ID(); ?>" />
<?php if (is_user_logged_in()) { ?>
                        <input type="hidden" name="user_id" value="<?php echo $user_ID; ?>" />
<?php } ?>
                    <input type="hidden" class="paypal_amount" name="amount" value="" />
                    <a class="goBack" style="cursor:pointer;" data-dismiss="modal">Go Back</a>
                    <button type="button" id="not-logged-in" class="yellowBtn">Donate Now</button>
                </footer>

            </form>

        </div>
    </div>
</div>
<div aria-labelledby="mySmallModalLabel" role="dialog" tabindex="-2" class="modal fade contact-cause-creator">
    <div class="modal-dialog donateModal">
        <div class="modal-content outLine">

            <form class="innerBox form-horizontal" id="contact-causeForm" action="<?php echo site_url(); ?>/ajax/contact-cause-creator.php" method="post">
                <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                    <span aria-hidden="false"></span>
                </button>
                <h2 style="text-align: center;margin-bottom: 10px;">Contact Cause Creator</h2>
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" placeholder="Enter your name" name="fullName" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" placeholder="Enter your email" name="email_id" class="form-control email_id">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <input type="text" placeholder="Subject" name="subject" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <textarea style="resize: vertical;" name="message" placeholder="Message" class="form-control"></textarea>
                    </div>
                </div>
                <footer class="formFoot">
                    <input type="hidden" name="post_id" value="<?php echo get_the_ID(); ?>" />
                    <span id="msg" style="color:#5f5;float: left;"></span>
                    <a class="goBack" style="cursor:pointer;" data-dismiss="modal">Go Back</a>
                    <button class="yellowBtn">Send</button>
                </footer>

            </form>

        </div>
    </div>
</div>
<form id="main-form" method="post" action="https://www.sandbox.paypal.com/cgi-bin/webscr" class="innerBox form-horizontal formStep-2">
    <input type="hidden" name="cmd" value="_donations">
    <input type="hidden" name="business" value="soumyadipdutta8@gmail.com">
    <input type="hidden" name="item_name" value="<?php echo the_title(); ?>">
    <input type="hidden" class="item_number" name="item_number" value="">
    <input class="paypal_amount" type="hidden" name="amount" value="">
    <input type="hidden" class="notify_url" name="notify_url" value="<?php echo site_url(); ?>/ipn/">    
    <input type="hidden" name="cancel_return" value="<?php echo get_permalink(); ?>">
    <input type="hidden" name="return" value="<?php echo site_url(); ?>/thank-you/">
</form>
<script src="http://code.jquery.com/jquery-1.7.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/share/platform.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/share/jquery.sharrre.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/share/twitter.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/share/facebook.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/share/linkedin.js" ></script>
<script>
    jQuery('#tw-share').sharrre({
        share: {
            twitter: true
        },
        buttons: {
            twitter: {
                hashtags: "SupportForPlanet"
            }
        },
        template: '<span class="s-twitter"></span><p>{total}</p>',
        enableTracking: true,
        render: function (api, options) {
            jQuery(api.element).on('click', '.s-twitter', function () {
                api.openPopup('twitter');
            });
        }
    });
    jQuery('#fb-share').sharrre({
        share: {
            facebook: true
        },
        buttons: {
            facebook: {}
        },
        template: '<span class="s-facebook"></span><p>{total}</p>',
        enableTracking: false,
        url: '<?php echo get_permalink(); ?>',
        render: function (api, options) {
            jQuery(api.element).on('click', '.s-facebook', function () {
                api.openPopup('facebook');
            });
        }
    });
    jQuery('#li-share').sharrre({
        share: {
            linkedin: true
        },
        buttons: {
            linkedin: {}
        },
        template: '<span class="s-inkt"></span><p>{total}</p>',
        enableTracking: false,
        url: '<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>',
        render: function (api, options) {
            jQuery(api.element).on('click', '.s-inkt', function () {
                api.openPopup('linkedin');
            });
        }
    });
</script>
<script type="text/javascript">
<!--
    jQuery('.rate_post').click(function () {

        jQuery.ajax({
            type: 'post',
            url: '<?php echo site_url() ?>/ajax/single_post_rating.php',
            data: 'mode=cause_rating&&loginuserid=<?php echo $login_user_id; ?>&&post_id=<?php echo get_the_ID() ?>',
            success: function (msg) {
                if (msg == 1)
                {
                    var single_cause_page_pathname = '<?php echo $single_cause_page_link; ?>';
                    window.location.href = single_cause_page_pathname;
                }
            }
        });

    });
    jQuery('#join_as_volunteer').click(function () {

        jQuery.ajax({
            type: 'post',
            url: '<?php echo site_url() ?>/ajax/join_volunteer.php',
            data: 'mode=join_volunteer&&user_id=<?php echo $login_user_id; ?>&&post_id=<?php echo get_the_ID() ?>',
            success: function (msg) {
                if (msg == '1')
                {
                    jQuery('.add_volunteer').html('<span style="color:#0c0;">You are successfully registered as volunteer</span>');
                }
            }
        });

    });

    jQuery('#donate-button').on('click', function () {
        if (jQuery('#enter-amount').val() == '') {
            jQuery('#enter-amount').addClass('error-msg');
            return false;
        }

<?php if (is_user_logged_in()) { ?>
            else{
                jQuery.ajax({
                    type: 'post',
                    url: '<?php echo site_url() ?>/ajax/donation.php',
                    data: jQuery('#paypal-form').serialize(),
                    success: function (data) {
                        if (data != '0')
                        {

                            jQuery('.item_number').val(data);
                            jQuery('#main-form').submit();
                            //alert(data);      
                        }
                    }
                });
                return false;
            }
<?php } ?>
    });
//-->    
</script>
<?php
wp_enqueue_script('single_cause_event');
get_footer();
?>