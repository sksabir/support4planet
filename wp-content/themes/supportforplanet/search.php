<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
get_header();
?>
<?php
$paged = ($_REQUEST['page2'] ? $_REQUEST['page2'] : 1);
global $query_string; // grab the search query
$wp_search_query = null;
?>

<div class="container featured">


    <div class="col-sm-12 col-md-12 view-cell">
        <div id="portfoliolist">
            <?php
            $wp_search_query = new WP_Query($query_string . '&post_type=cause&posts_per_page=1&paged=' . $paged); //
            //'post_status' => 'any',  
            ?>


            <?php if ($wp_search_query->have_posts()) : ?>
                <?php /* Start the Loop */ ?>
                <?php while ($wp_search_query->have_posts()) : $wp_search_query->the_post(); ?>
                    <?php
                    $imgurl = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full');
                    if ($imgurl[0] != '') {
                        $img = $imgurl[0];
                    } else {
                        $img = get_template_directory_uri() . "/images/no-img.png";
                    }
                    ?>


                    <div class="portfolio app mix_all">
                        <div class="featured-causes-box">
                            <a href="<?php the_permalink(); ?>"><img alt="<?php the_title(); ?>" src="<?php echo $img; ?>"></a>           
                            <div class="docoment-section">
                                <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                <p><?php echo get_the_popular_excerpt(get_the_content(), 100); ?> </p>                 
                            </div>
                            <div class="grey-support">
                                <ul>
                                    <li><span class="rupee"></span>
                                        <p>Funded 80%</p>
                                    </li>
                                    <li><span class="doller"></span>
                                        <p>Pledged $2453</p>
                                    </li>
                                </ul>
                                <div class="supprot"><a href="">Support</a></div>
                            </div>
                        </div>
                    </div>  

                <?php endwhile; ?>
                <?php
                $blog_cause_paginate = pagination($wp_search_query);

                if (!empty($blog_cause_paginate)) {
                    echo '<div class="pag-block">';
                    echo $blog_cause_paginate;
                    echo '</div>';
                }
                $wp_search_query = null;
                wp_reset_postdata();
                ?>

            <?php else : ?>

                <div class="errorsearcmsg"> <h2>Sorry! No cause Found.</h2></div>

            <?php endif; ?>





        </div>
    </div>

</div>
<!-- Featured end --> 

<?php //get_sidebar();  ?>
<?php get_footer(); ?>
