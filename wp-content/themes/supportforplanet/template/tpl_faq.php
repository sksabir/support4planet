<?php
/**
 * Template Name:FAQ template
 */
get_header();
?>


<!-- Featured -->
<div class="container donor-comment-section">
    <div class="row">
        <!-- Graph TK --> 
        <div class="col-sm-12 col-md-8 col-md-offset-2 graph-tk">
            <h2><?php echo get_field("title"); ?></h2>
            <div class="col-sm-12 col-md-5 login-section-two">
                <form role="search" method="get" id="searchform"
                      class="searchform" action="<?php echo esc_url(home_url('/')); ?>">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-xs-12">
                            <div class="input-group">
                              <!-- <label class="screen-reader-text" for="s"><?php _x('Search for:', 'label'); ?></label> -->
                                <input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" class="form-control input-search" placeholder="Search Cause"/>
                                <span class="input-group-btn">
                                    <input type="submit" id="searchsubmit" class="btn btn-primary"  value="" />
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 tab-content">
            <h3><?php echo get_field("title_new_users"); ?></h3>
            <div id="parentHorizontalTab">
                <ul class="resp-tabs-list hor_1">
                    <li><?php echo get_field("title_new_users"); ?></li>
                    <li class="active"><?php echo get_field("title_why_support4planet"); ?></li>
                    <li><?php echo get_field("title_how_much_can_i_donate"); ?></li>
                    <li><?php echo get_field("title_tax_deduction_certificate"); ?></li>
                    <li><?php echo get_field("title_i_dont_want_my_name_in_donors_list"); ?></li>
                </ul>
                <div class="resp-tabs-container hor_1">
                    <!-- Community Three -->
                    <div class="community-three">
                        <?php echo get_field("description_general_information"); ?>
                    </div>
                    <!-- Community Three -->
                    <!-- Community Two -->
                    <div class="community-three">
                        <?php echo get_field("description_why_support4planet"); ?>
                    </div>
                    <div class="community-three">
                        <?php echo get_field("description_how_much_can_i_donate"); ?>
                    </div>
                    <div class="community-three">
                        <?php echo get_field("description_tax_deduction_certificate"); ?>
                    </div>
                    <div class="community-three">
                        <?php echo get_field("description_i_dont_want_my_name_in_donors_list"); ?>
                    </div>
                </div>
            </div>
        </div>




        <!-- Featured end --> 
        <?php get_footer(); ?>