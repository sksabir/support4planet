<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title>Support Planet</title>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.theme.default.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/easy-responsive-tabs.css"/>
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <!-- header -->
        <header>
            <div class="container-fluid header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3"><a href="<?php echo site_url(); ?>" class="logo"><?php show_easylogo(); ?></a></div>
                        <div class="col-sm-9 right-login-section">
                            <div class="col-md-12 no-padding">
                                <div class="user_login">
                                    <ul>
                                        <li><a href="" >Login </a></li>
                                        <li><a href="<?php echo get_the_permalink(15); ?>"> Create Account</a></li>
                                    </ul>
                                    <button type="button" class="btn btn-primary"> Donate</button>
                                </div>
                            </div>
                            <div class="col-md-12 no-padding">
                                <div class="col-sm-5 no-padding">
                                    <form role="search" method="get" id="searchform"
                                          class="searchform" action="<?php echo esc_url(home_url('/')); ?>">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12 col-xs-12">
                                                <div class="input-group">
                                                    <!-- <label class="screen-reader-text" for="s"><?php _x('Search for:', 'label'); ?></label> -->
                                                    <input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" class="form-control input-search" placeholder="Search Cause"/>
                                                    <span class="input-group-btn">
                                                        <input type="submit" id="searchsubmit" class="btn btn-primary"	value="" />
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-7 planetnav no-padding">
                                    <nav class="navbar navbar-default"> 
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                                        </div>

                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <!--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">-->
                                        <!-- <ul class="nav navbar-nav">
                                          <li class="active"><a href="index.html">Home</a></li>
                                          <li><a href="about.html"> About Us </a></li>
                                          <li><a href="cause.html">Causes </a></li>
                                          <li><a href="start_cause.html"> Start A Cause</a></li>
                                          <li><a href="contact.html"> Contact Us</a></li>
                                        </ul> -->
                                        <?php
                                        wp_nav_menu(array(
                                            'theme_location' => 'primary',
                                            'container_class' => 'collapse navbar-collapse',
                                            'container_id' => 'bs-example-navbar-collapse-1',
                                            'menu_class' => 'nav navbar-nav'
                                        ));
                                        ?>
                                        <!--</div>-->
                                        <!-- /.navbar-collapse --> 
                                        <!-- /.container-fluid --> 
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>
    <!-- header end --> 