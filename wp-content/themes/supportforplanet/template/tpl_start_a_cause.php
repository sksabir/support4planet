<?php
/**
 * Template Name:Start A Cause template
 */
get_header();
?>
<!-- cause-banner -->
<div class="container-fluid start_cause-banner">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h2><?php echo get_field("title"); ?></h2>
                <?php echo get_field("description"); ?>
            </div>
        </div>
    </div>
</div>
<!-- cause-banner end --> 
<!-- Featured -->
<div class="container featured">
    <div class="row">
        <!-- Graph TK --> 
        <div class="col-sm-12 col-md-8 col-md-offset-2 graph-tk">
            <h2><?php echo get_field("title_join_us"); ?></h2>
            <?php echo get_field("description_join_us"); ?>
        </div>
        <!-- Graph TK End -->
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12 video">
            <h2><?php echo get_field("title"); ?></h2>
            <?php echo get_field("video_iframe"); ?> 

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12 stakit">
            <ul>
                <li><span><img src="<?php echo get_field("image_who_you_are"); ?>"  alt=""/></span>
                    <h2><a href="<?php echo get_field("link_who_you_are"); ?>"> <?php echo get_field("title_who_you_are"); ?> </a></h2> <?php echo get_field("description_who_you_are"); ?> </li>

                <li><span><img src="<?php echo get_field("image_create_your_cause"); ?>"  alt=""/></span>
                    <h2><a href="<?php echo get_field("link_create_your_cause"); ?>"> <?php echo get_field("title_create_your_cause"); ?> </a></h2> <?php echo get_field("description_create_your_cause"); ?> </li>

                <li><span><img src="<?php echo get_field("image_publish_your_cause"); ?>"  alt=""/></span>
                    <h2><a href="<?php echo get_field("link_publish_your_cause"); ?>"><?php echo get_field("title_publish_your_cause"); ?></a></h2><?php echo get_field("description_publish_your_cause"); ?></li>

                <li><span><img src="<?php echo get_field("image_build_your_community"); ?>"  alt=""/></span>
                    <h2><a href="<?php echo get_field("link_build_your_community"); ?>"><?php echo get_field("title_build_your_community"); ?></a></h2><?php echo get_field("description_build_your_community"); ?></li>

                <li><span><img src="<?php echo get_field("image_track_your_progress"); ?>"  alt=""/></span>
                    <h2><a href="<?php echo get_field("link_track_your_progress"); ?>"><?php echo get_field("title_track_your_progress"); ?></a></h2><?php echo get_field("description_track_your_progress"); ?></li>

                <li><span><img src="<?php echo get_field("image_update_your_supporters"); ?>"  alt=""/></span>
                    <h2><a href="<?php echo get_field("link_update_your_supporters"); ?>"><?php echo get_field("title_update_your_supporters"); ?></a></h2><?php echo get_field("description_update_your_supporters"); ?></li>
            </ul>
        </div>
    </div>

</div>

<div class="container-fluid becoming">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-md-offset-2">
                <h2><?php echo get_field("title_becoming_a_partner"); ?></h2>
                <?php echo get_field("description_becoming_a_partner"); ?>
                <a class="join-cause" href="<?php echo get_field("button_link_becoming_a_partner"); ?>"><?php echo get_field("button_level_becoming_a_partner"); ?></a>
            </div>
        </div>
    </div>

</div>

<!-- Featured end --> 
<?php get_footer(); ?>