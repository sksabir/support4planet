<?php
/**
 * Template Name:Login template
 */
if (is_user_logged_in()) {
    $user_ID = get_current_user_id();
    $user = get_userdata($user_ID);
    $roles = implode(', ', $user->roles);
    //print_r($user);
    if ($roles == 'organization') {
        ?>
        <script type="text/javascript">window.location.href = "<?php echo site_url(); ?>/organizer-dashboard/";</script>
        <?php
    } else if ($roles == 'individual') {
        ?>
        <script type="text/javascript">window.location.href = "<?php echo site_url(); ?>/individual-dashboard/";</script>
        <?php
    }
}
$msg = '';
if ($_POST['email_id'] != '' && $_POST['pass'] != '') {
    $creds = array();
    $creds['user_login'] = $_POST['email_id'];
    $creds['user_password'] = $_POST['pass'];
    $creds['remember'] = false;
    $user = wp_signon($creds, false);
    if (is_wp_error($user)) {
        $err = $user->get_error_message();
        $msg = '<p style="color:#c00;">Username or Password is incorrect.</p>';
    } else {
        $roles = implode(', ', $user->roles);
        //print_r($roles);
        if ($roles == 'organization') {
            $admin_link = site_url() . "/organizer-dashboard/";
        } else if ($roles == 'individual') {
            $admin_link = site_url() . "/individual-dashboard/";
        }
        $msg = '<p style="color:#0c0;">You are successfully logged in</p><script type="text/javascript">window.location.href = "' . $admin_link . '";</script>';
    }
}
get_header();
wp_reset_query();
?>
<style type="text/css">
    .error-msg {
        border: 1px solid red !important;
    }
    .error {
        color: red;
    }
</style>
<section class="loginSec">
    <div class="container">

        <h2><?php the_title(); ?></h2>
        <?php echo $msg; ?>
        <p><?php the_content(); ?></p>
        <div class="loginformWrap">
            <form class="loginForm form-horizontal" action="" method="post">
                <div class="form-group">
                    <label class="col-sm-4 control-label">Email:</label>
                    <div class="col-sm-8">
                        <input type="text" name="email_id" placeholder="Enter the email address" class="form-control email_id">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Password:</label>
                    <div class="col-sm-8">
                        <input type="password" name="pass" placeholder="****" class="form-control">
                        <p><a href="<?php echo site_url(); ?>/forget-password/">Forgot Password?</a></p>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4">
                        <p class="smText">Doesn’t have an account? <a href="<?php echo site_url(); ?>/create-account/">Sign Up</a></p>
                    </div>
                    <div class="col-sm-4">
                        <input type="submit" value="Login" class="btn btn-primary"> 
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- login end --> 
<?php
wp_enqueue_script('user_login');
get_footer();
?>