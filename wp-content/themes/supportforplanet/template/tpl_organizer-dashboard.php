<?php
/**
 * Template Name:Organizer Dashboard template
 */
if (!is_user_logged_in()) {
    ?>
    <script type="text/javascript">window.location.href = "<?php echo site_url(); ?>/log-in/";</script>
    <?php
} else {
    $user_ID = get_current_user_id();
    $user = get_userdata($user_ID);
    $fName = get_user_meta($user_ID, 'first_name', true);
    $lName = get_user_meta($user_ID, 'last_name', true);
    $roles = implode(', ', $user->roles);
    //print_r($user);
    if ($roles != 'organization') {
        ?>
        <script type="text/javascript">window.location.href = "<?php echo site_url(); ?>";</script>
        <?php
    }
}
get_header();
?>
<div class="container donor-comment-section">
    <div class="row"> 
        <!-- Graph TK -->
        <div class="col-sm-12 col-md-12 col-md-offset-0 graph-tk">
            <div class="get-form">
                <div class="organisation">
                    <div class="col-sm-12 col-md-3 organisation-left">
                        <?php
                        $pic = get_field('profile_picture', 'user_' . $user_ID);
                        if (!empty($pic)) {
                            $profile_picture = get_field('profile_picture', 'user_' . $user_ID);
                            $url = $profile_picture['url'];
                            $alt = $profile_picture['alt'];
                        } else {
                            $url = get_template_directory_uri() . '/images/blank.jpg';
                            $alt = '';
                        }
                        ?>
                        <div class="charitable"> <span style="background:url('<?php echo $url; ?>') no-repeat scroll 0 0 / cover;" class="curcal-orenge "></span>
                            <div class="chari-right">
                                <h4><?php echo get_field('organization_name', 'user_' . $user_ID); ?></h4>
                                <h3><?php echo get_field('city', 'user_' . $user_ID); ?>, <?php echo get_field('state', 'user_' . $user_ID); ?></h3>
                                <?php $user_registered = $user->user_registered; ?>
                                <p class="author">Joined on <?php echo date('M j, Y', strtotime($user_registered)); ?></p>
                            </div>
                        </div>
                        <div class="infotech-location">
                            <h2>Details</h2>
                            <div class="infotech_info">
                                <strong>Website</strong>
                                <span><a href="<?php echo $user->user_url; ?>"><?php echo $user->user_url; ?></a></span>
                                <br class="clearfix">
                                <strong>Address</strong>
                                <span><?php echo get_field('street_address', 'user_' . $user_ID); ?>, <?php echo get_field('city', 'user_' . $user_ID); ?>, <?php echo get_field('state', 'user_' . $user_ID); ?>, <?php echo get_field('zip', 'user_' . $user_ID); ?></span>
                                <br class="clearfix">
                                <strong>Phone</strong>
                                <span><a href="tel:<?php echo get_field('phone_number', 'user_' . $user_ID); ?>"><?php echo get_field('phone_number', 'user_' . $user_ID); ?></a></span>
                                <br class="clearfix">
                                <strong>Email</strong>
                                <span><a href="#"><?php echo $user->user_email; ?></a></span>
                                <br class="clearfix">
                                <strong>Admin</strong>
                                <span><?php echo $fName . ' ' . $lName; ?></span>

                            </div>
                            <div class="form-group">
                                <!-- <div class="col-sm-12 col-md-12 no-padding">
                                  <button class="btn btn-primary" name="singlebutton" id="singlebutton">Contact Cause Creator</button>
                                </div> -->
                            </div>
                        </div>
                        <div class="infotech-location">
                            <h2>Amount Raised Till Date</h2>
                            <ul>
                                <li><span><img src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon.png"  alt=""/></span>
                                    <div class="pirce-m">
                                        <h2>$1,782,009</h2>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-9 cause_created">
                        <div id="parentHorizontalTab">
                            <ul class="resp-tabs-list hor_1">
                                <li class="active">Cause Created</li>
                                <li>Recent Causes</li>
                                <li>Event Created</li>
                                <li>Recent Events</li>
                                <li>Donor List</li>
                                <li>Volunteer List</li>
                            </ul>
                            <a href="<?php echo site_url(); ?>/join-cause/" class="yellowBtn">Create A Cause</a>
                            <div class="resp-tabs-container hor_1">
                                <!-- Community Three -->
                                <div class="community-three">
                                    <?php
                                    $args = array(
                                        'posts_per_page' => -1,
                                        'orderby' => 'date',
                                        'order' => 'ASC',
                                        'post_type' => 'cause',
                                        'author' => $user_ID,
                                        'post_status' => 'publish',
                                        'suppress_filters' => true
                                    );
                                    $posts_array = get_posts($args);
//echo '<pre>';
//print_r($posts_array);
//echo '</pre>';
                                    foreach ($posts_array as $post) {
                                        ?>
                                        <div class="created_list">
                                            <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
                                            <img src="<?php echo $url; ?>"  alt="<?php echo $post->post_title; ?>"/>
                                            <div class="created_list_right">
                                                <h2><?php echo $post->post_title; ?></h2>
                                                <p><?php echo wordlimit($post->post_content, 40); ?></p>
                                                <ul>
                                                    <li>
                                                        <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon.png"></span>
                                                        <div class="pirce-c">
                                                            <h2>$241,000</h2>
                                                            <p>$<?php echo number_format(get_field('fundraising_goal', $post->ID)); ?> goal</p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon2.png"></span>
                                                        <div class="pirce-c">
                                                            <h2>894</h2>
                                                            <p>donors</p>
                                                        </div>
                                                    </li>
                                                    <?php
                                                    $end_date = get_field('end_date_time', $post->ID);
                                                    $now = time(); // or your date as well
                                                    $end_date = strtotime($end_date);
                                                    $datediff = floor(($end_date - $now) / (60 * 60 * 24));
                                                    if ($datediff >= 0) {
                                                        $left = '<h2>' . $datediff . '</h2><p>days left</p>';
                                                    } else {
                                                        $left = '<h2>Expired!!!</h2>';
                                                    }
                                                    ?>
                                                    <li>
                                                        <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon3.png"></span>
                                                        <div class="pirce-c">
                                                            <?php echo $left; ?>
                                                        </div>
                                                    </li>
                                                </ul>           
                                            </div>             
                                        </div>
                                    <?php } ?>             
                                </div>
                                <div class="community-three">

                                    <?php
                                    $args = array(
                                        'posts_per_page' => 5,
                                        'orderby' => 'date',
                                        'order' => 'DESC',
                                        'post_type' => 'cause',
                                        'author' => $user_ID,
                                        'post_status' => 'publish',
                                        'suppress_filters' => true
                                    );
                                    $posts_array = get_posts($args);
                                    //echo '<pre>';
                                    //print_r($posts_array);
                                    //echo '</pre>';
                                    foreach ($posts_array as $post) {
                                        ?>
                                        <div class="created_list">
                                            <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
                                            <img src="<?php echo $url; ?>"  alt="<?php echo $post->post_title; ?>"/>
                                            <div class="created_list_right">
                                                <h2><?php echo $post->post_title; ?></h2>
                                                <p><?php echo wordlimit($post->post_content, 40); ?></p>
                                                <ul>
                                                    <li>
                                                        <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon.png"></span>
                                                        <div class="pirce-c">
                                                            <h2>$241,000</h2>
                                                            <p>$<?php echo number_format(get_field('fundraising_goal', $post->ID)); ?> goal</p>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon2.png"></span>
                                                        <div class="pirce-c">
                                                            <h2>894</h2>
                                                            <p>donors</p>
                                                        </div>
                                                    </li>
                                                    <?php
                                                    $end_date = get_field('end_date_time', $post->ID);
                                                    $now = time(); // or your date as well
                                                    $end_date = strtotime($end_date);
                                                    $datediff = floor(($end_date - $now) / (60 * 60 * 24));
                                                    if ($datediff >= 0) {
                                                        $left = '<h2>' . $datediff . '</h2><p>days left</p>';
                                                    } else {
                                                        $left = '<h2>Expired!!!</h2>';
                                                    }
                                                    ?>
                                                    <li>
                                                        <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon3.png"></span>
                                                        <div class="pirce-c">
                                                            <?php echo $left; ?>
                                                        </div>
                                                    </li>
                                                </ul>           
                                            </div>             
                                        </div>
                                    <?php } ?>
                                </div>
                                <!-- Community Three -->
                                <!-- Community Two -->



                                <div class="community-three">
                                    <?php
                                    $args = array(
                                        'posts_per_page' => -1,
                                        'orderby' => 'date',
                                        'order' => 'ASC',
                                        'post_type' => 'event',
                                        'author' => $user_ID,
                                        'post_status' => 'publish',
                                        'suppress_filters' => true
                                    );
                                    $posts_array = get_posts($args);
                                    //echo '<pre>';
                                    //print_r($posts_array);
                                    //echo '</pre>';
                                    foreach ($posts_array as $post) {
                                        ?>
                                        <div class="created_list">
                                            <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
                                            <img src="<?php echo $url; ?>"  alt="<?php echo $post->post_title; ?>"/>
                                            <div class="created_list_right">
                                                <h2><?php echo $post->post_title; ?></h2>
                                                <p><?php echo wordlimit($post->post_content, 40); ?></p>
                                                <ul>
                                                    <?php if (get_field('fundraising_goal_event', $post->ID) != '') { ?>
                                                        <li>
                                                            <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon.png"></span>
                                                            <div class="pirce-c">
                                                                <h2>$241,000</h2>
                                                                <p>$<?php echo number_format(get_field('fundraising_goal_event', $post->ID)); ?> goal</p>
                                                            </div>
                                                        </li>
                                                    <?php } ?>
                                                    <li>
                                                        <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon2.png"></span>
                                                        <div class="pirce-c">
                                                            <h2>894</h2>
                                                            <p>donors</p>
                                                        </div>
                                                    </li>
                                                    <?php
                                                    $end_date = get_field('end_date_time', $post->ID);
                                                    $now = time(); // or your date as well
                                                    $end_date = strtotime($end_date);
                                                    $datediff = floor(($end_date - $now) / (60 * 60 * 24));
                                                    if ($datediff >= 0) {
                                                        $left = '<h2>' . $datediff . '</h2><p>days left</p>';
                                                    } else {
                                                        $left = '<h2>Expired!!!</h2>';
                                                    }
                                                    ?>
                                                    <li>
                                                        <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon3.png"></span>
                                                        <div class="pirce-c">
                                                            <?php echo $left; ?>
                                                        </div>
                                                    </li>
                                                    <?php if (get_field('no_volunteers', $post->ID) != '') { ?>
                                                        <li>
                                                            <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/volunteers.png"></span>
                                                            <div class="pirce-c">
                                                                <h2>2</h2>
                                                                <p>needed <?php echo get_field('no_volunteers', $post->ID); ?> volunteers</p>
                                                            </div>
                                                        </li>
                                                    <?php } ?>
                                                </ul>           
                                            </div>             
                                        </div>
                                    <?php } ?>             
                                </div>
                                <div class="community-three">

                                    <?php
                                    $args = array(
                                        'posts_per_page' => 5,
                                        'orderby' => 'date',
                                        'order' => 'DESC',
                                        'post_type' => 'event',
                                        'author' => $user_ID,
                                        'post_status' => 'publish',
                                        'suppress_filters' => true
                                    );
                                    $posts_array = get_posts($args);
//echo '<pre>';
//print_r($posts_array);
//echo '</pre>';
                                    foreach ($posts_array as $post) {
                                        ?>
                                        <div class="created_list">
                                            <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID)); ?>
                                            <img src="<?php echo $url; ?>"  alt="<?php echo $post->post_title; ?>"/>
                                            <div class="created_list_right">
                                                <h2><?php echo $post->post_title; ?></h2>
                                                <p><?php echo wordlimit($post->post_content, 40); ?></p>
                                                <ul>
                                                    <?php if (get_field('fundraising_goal_event', $post->ID) != '') { ?>
                                                        <li>
                                                            <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon.png"></span>
                                                            <div class="pirce-c">
                                                                <h2>$241,000</h2>
                                                                <p>$<?php echo number_format(get_field('fundraising_goal_event', $post->ID)); ?> goal</p>
                                                            </div>
                                                        </li>
                                                    <?php } ?>
                                                    <li>
                                                        <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon2.png"></span>
                                                        <div class="pirce-c">
                                                            <h2>894</h2>
                                                            <p>donors</p>
                                                        </div>
                                                    </li>
                                                    <?php
                                                    $end_date = get_field('end_date_time', $post->ID);
                                                    $now = time(); // or your date as well
                                                    $end_date = strtotime($end_date);
                                                    $datediff = floor(($end_date - $now) / (60 * 60 * 24));
                                                    if ($datediff >= 0) {
                                                        $left = '<h2>' . $datediff . '</h2><p>days left</p>';
                                                    } else {
                                                        $left = '<h2>Expired!!!</h2>';
                                                    }
                                                    ?>
                                                    <li>
                                                        <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon3.png"></span>
                                                        <div class="pirce-c">
                                                            <?php echo $left; ?>
                                                        </div>
                                                    </li>
                                                    <?php if (get_field('no_volunteers', $post->ID) != '') { ?>
                                                        <li>
                                                            <span><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/volunteers.png"></span>
                                                            <div class="pirce-c">
                                                                <h2>2</h2>
                                                                <p>needed <?php echo get_field('no_volunteers', $post->ID); ?> volunteers</p>
                                                            </div>
                                                        </li>
                                                    <?php } ?>
                                                </ul>           
                                            </div>             
                                        </div>
                                    <?php } ?>
                                </div>





                                <div class="community-three">
                                    <form>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label for="exampleInputEmail1">Filter Donor by Cause:</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <select id="country" name="country" class="form-control selectpicker">
                                                    <option>Tk's Memorial Funds</option>
                                                    <option>Throw Me a Bone, Mista!</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="filter-list">
                                        <ul>

                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/th-1.jpg"  alt=""/></span>
                                                <h2><a href="">James Franco</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/th-2.jpg"  alt=""/></span>
                                                <h2><a href="">Samantha Carter</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/th-3.jpg"  alt=""/></span>
                                                <h2><a href="">Brina Lee</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/th-4.jpg"  alt=""/></span>
                                                <h2><a href="">John Carter</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/th-5.jpg"  alt=""/></span>
                                                <h2><a href="">Rosalind Ryne</h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/aoewd.jpg"  alt=""/></span>
                                                <h2><a href="">Roger Kitsch</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/th-6.jpg"  alt=""/></span>
                                                <h2><a href="">Hope For Paw</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/aoewd.jpg"  alt=""/></span>
                                                <h2><a href="">Jennifer Jones</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/dog.jpg"  alt=""/></span>
                                                <h2><a href="">Tom Lautner</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/th-7.jpg"  alt=""/></span>
                                                <h2><a href="">Jessie Maclow</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/aoewd.jpg"  alt=""/></span>
                                                <h2><a href="">Kevin Richardson</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/th-8.jpg"  alt=""/></span>
                                                <h2><a href="">Emily Clark</a></h2>
                                            </li>
                                        </ul>
                                    </div>

                                </div>

                                <div class="community-three">
                                    <form>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <label for="exampleInputEmail1">Filter Donor by Cause:</label>
                                            </div>
                                            <div class="col-sm-9">
                                                <select id="country" name="country" class="form-control selectpicker">
                                                    <option>Tk's Memorial Funds</option>
                                                    <option>Throw Me a Bone, Mista!</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="filter-list">
                                        <ul>


                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/th-4.jpg"  alt=""/></span>
                                                <h2><a href="">John Carter</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/th-5.jpg"  alt=""/></span>
                                                <h2><a href="">Rosalind Ryne</h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/aoewd.jpg"  alt=""/></span>
                                                <h2><a href="">Roger Kitsch</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/th-6.jpg"  alt=""/></span>
                                                <h2><a href="">Hope For Paw</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/aoewd.jpg"  alt=""/></span>
                                                <h2><a href="">Jennifer Jones</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/dog.jpg"  alt=""/></span>
                                                <h2><a href="">Tom Lautner</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/th-7.jpg"  alt=""/></span>
                                                <h2><a href="">Jessie Maclow</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/aoewd.jpg"  alt=""/></span>
                                                <h2><a href="">Kevin Richardson</a></h2>
                                            </li>
                                            <li>
                                                <span><img src="<?php echo get_template_directory_uri(); ?>/images/th-8.jpg"  alt=""/></span>
                                                <h2><a href="">Emily Clark</a></h2>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                                <!-- Community Two End -->

                            </div>
                        </div></div>


                </div>
            </div>

        </div>
    </div>
</div>

<?php
get_footer();
?>