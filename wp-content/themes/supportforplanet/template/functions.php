<?php

/**
 * Twenty Sixteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @link https://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * {@link https://codex.wordpress.org/Plugin_API}
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
/**
 * Twenty Sixteen only works in WordPress 4.4 or later.
 */
if (version_compare($GLOBALS['wp_version'], '4.4-alpha', '<')) {
    require get_template_directory() . '/inc/back-compat.php';
}

function po_session() {
    if (!session_id()) {
        session_start();
    }
}

add_action('init', 'po_session');


if (!function_exists('twentysixteen_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * Create your own twentysixteen_setup() function to override in a child theme.
     *
     * @since Twenty Sixteen 1.0
     */
    function twentysixteen_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Twenty Sixteen, use a find and replace
         * to change 'twentysixteen' to the name of your theme in all the template files
         */
        load_theme_textdomain('twentysixteen', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */
        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(1200, 9999);

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus(array(
            'primary' => __('Primary Menu', 'twentysixteen'),
            'social' => __('Social Links Menu', 'twentysixteen'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'status',
            'audio',
            'chat',
        ));

        /*
         * This theme styles the visual editor to resemble the theme style,
         * specifically font, colors, icons, and column width.
         */
        add_editor_style(array('css/editor-style.css', twentysixteen_fonts_url()));
    }

endif; // twentysixteen_setup
add_action('after_setup_theme', 'twentysixteen_setup');

/**
 * Sets the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_content_width() {
    $GLOBALS['content_width'] = apply_filters('twentysixteen_content_width', 840);
}

add_action('after_setup_theme', 'twentysixteen_content_width', 0);

/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_widgets_init() {
    register_sidebar(array(
        'name' => __('Sidebar', 'twentysixteen'),
        'id' => 'sidebar-1',
        'description' => __('Add widgets here to appear in your sidebar.', 'twentysixteen'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => __('Content Bottom 1', 'twentysixteen'),
        'id' => 'sidebar-2',
        'description' => __('Appears at the bottom of the content on posts and pages.', 'twentysixteen'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));

    register_sidebar(array(
        'name' => __('Content Bottom 2', 'twentysixteen'),
        'id' => 'sidebar-3',
        'description' => __('Appears at the bottom of the content on posts and pages.', 'twentysixteen'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'twentysixteen_widgets_init');

if (!function_exists('twentysixteen_fonts_url')) :

    /**
     * Register Google fonts for Twenty Sixteen.
     *
     * Create your own twentysixteen_fonts_url() function to override in a child theme.
     *
     * @since Twenty Sixteen 1.0
     *
     * @return string Google fonts URL for the theme.
     */
    function twentysixteen_fonts_url() {
        $fonts_url = '';
        $fonts = array();
        $subsets = 'latin,latin-ext';

        /* translators: If there are characters in your language that are not supported by Merriweather, translate this to 'off'. Do not translate into your own language. */
        if ('off' !== _x('on', 'Merriweather font: on or off', 'twentysixteen')) {
            $fonts[] = 'Merriweather:400,700,900,400italic,700italic,900italic';
        }

        /* translators: If there are characters in your language that are not supported by Montserrat, translate this to 'off'. Do not translate into your own language. */
        if ('off' !== _x('on', 'Montserrat font: on or off', 'twentysixteen')) {
            $fonts[] = 'Montserrat:400,700';
        }

        /* translators: If there are characters in your language that are not supported by Inconsolata, translate this to 'off'. Do not translate into your own language. */
        if ('off' !== _x('on', 'Inconsolata font: on or off', 'twentysixteen')) {
            $fonts[] = 'Inconsolata:400';
        }

        if ($fonts) {
            $fonts_url = add_query_arg(array(
                'family' => urlencode(implode('|', $fonts)),
                'subset' => urlencode($subsets),
                    ), 'https://fonts.googleapis.com/css');
        }

        return $fonts_url;
    }

endif;

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Sixteen 1.0
 */
function twentysixteen_javascript_detection() {
    echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}

add_action('wp_head', 'twentysixteen_javascript_detection', 0);

/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
// function twentysixteen_scripts() {
// 	// Add custom fonts, used in the main stylesheet.
// 	wp_enqueue_style( 'twentysixteen-fonts', twentysixteen_fonts_url(), array(), null );
// 	// Add Genericons, used in the main stylesheet.
// 	wp_enqueue_style( 'genericons', get_template_directory_uri() . '/genericons/genericons.css', array(), '3.4.1' );
// 	// Theme stylesheet.
// 	wp_enqueue_style( 'twentysixteen-style', get_stylesheet_uri() );
// 	// Load the Internet Explorer specific stylesheet.
// 	wp_enqueue_style( 'twentysixteen-ie', get_template_directory_uri() . '/css/ie.css', array( 'twentysixteen-style' ), '20150930' );
// 	wp_style_add_data( 'twentysixteen-ie', 'conditional', 'lt IE 10' );
// 	// Load the Internet Explorer 8 specific stylesheet.
// 	wp_enqueue_style( 'twentysixteen-ie8', get_template_directory_uri() . '/css/ie8.css', array( 'twentysixteen-style' ), '20151230' );
// 	wp_style_add_data( 'twentysixteen-ie8', 'conditional', 'lt IE 9' );
// 	// Load the Internet Explorer 7 specific stylesheet.
// 	wp_enqueue_style( 'twentysixteen-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'twentysixteen-style' ), '20150930' );
// 	wp_style_add_data( 'twentysixteen-ie7', 'conditional', 'lt IE 8' );
// 	// Load the html5 shiv.
// 	wp_enqueue_script( 'twentysixteen-html5', get_template_directory_uri() . '/js/html5.js', array(), '3.7.3' );
// 	wp_script_add_data( 'twentysixteen-html5', 'conditional', 'lt IE 9' );
// 	wp_enqueue_script( 'twentysixteen-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151112', true );
// 	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
// 		wp_enqueue_script( 'comment-reply' );
// 	}
// 	if ( is_singular() && wp_attachment_is_image() ) {
// 		wp_enqueue_script( 'twentysixteen-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20151104' );
// 	}
// 	wp_enqueue_script( 'twentysixteen-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20151204', true );
// 	wp_localize_script( 'twentysixteen-script', 'screenReaderText', array(
// 		'expand'   => __( 'expand child menu', 'twentysixteen' ),
// 		'collapse' => __( 'collapse child menu', 'twentysixteen' ),
// 	) );
// }
// add_action( 'wp_enqueue_scripts', 'twentysixteen_scripts' );

/**
 * Adds custom classes to the array of body classes.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $classes Classes for the body element.
 * @return array (Maybe) filtered body classes.
 */
function twentysixteen_body_classes($classes) {
    // Adds a class of custom-background-image to sites with a custom background image.
    if (get_background_image()) {
        $classes[] = 'custom-background-image';
    }

    // Adds a class of group-blog to sites with more than 1 published author.
    if (is_multi_author()) {
        $classes[] = 'group-blog';
    }

    // Adds a class of no-sidebar to sites without active sidebar.
    if (!is_active_sidebar('sidebar-1')) {
        $classes[] = 'no-sidebar';
    }

    // Adds a class of hfeed to non-singular pages.
    if (!is_singular()) {
        $classes[] = 'hfeed';
    }

    return $classes;
}

add_filter('body_class', 'twentysixteen_body_classes');

/**
 * Converts a HEX value to RGB.
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $color The original color, in 3- or 6-digit hexadecimal form.
 * @return array Array containing RGB (red, green, and blue) values for the given
 *               HEX code, empty array otherwise.
 */
function twentysixteen_hex2rgb($color) {
    $color = trim($color, '#');

    if (strlen($color) === 3) {
        $r = hexdec(substr($color, 0, 1) . substr($color, 0, 1));
        $g = hexdec(substr($color, 1, 1) . substr($color, 1, 1));
        $b = hexdec(substr($color, 2, 1) . substr($color, 2, 1));
    } else if (strlen($color) === 6) {
        $r = hexdec(substr($color, 0, 2));
        $g = hexdec(substr($color, 2, 2));
        $b = hexdec(substr($color, 4, 2));
    } else {
        return array();
    }

    return array('red' => $r, 'green' => $g, 'blue' => $b);
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images
 *
 * @since Twenty Sixteen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentysixteen_content_image_sizes_attr($sizes, $size) {
    $width = $size[0];

    840 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 62vw, 840px';

    if ('page' === get_post_type()) {
        840 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
    } else {
        840 > $width && 600 <= $width && $sizes = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 61vw, (max-width: 1362px) 45vw, 600px';
        600 > $width && $sizes = '(max-width: ' . $width . 'px) 85vw, ' . $width . 'px';
    }

    return $sizes;
}

add_filter('wp_calculate_image_sizes', 'twentysixteen_content_image_sizes_attr', 10, 2);

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails
 *
 * @since Twenty Sixteen 1.0
 *
 * @param array $attr Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size Registered image size or flat array of height and width dimensions.
 * @return string A source size value for use in a post thumbnail 'sizes' attribute.
 */
function twentysixteen_post_thumbnail_sizes_attr($attr, $attachment, $size) {
    if ('post-thumbnail' === $size) {
        is_active_sidebar('sidebar-1') && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 984px) 60vw, (max-width: 1362px) 62vw, 840px';
        !is_active_sidebar('sidebar-1') && $attr['sizes'] = '(max-width: 709px) 85vw, (max-width: 909px) 67vw, (max-width: 1362px) 88vw, 1200px';
    }
    return $attr;
}

add_filter('wp_get_attachment_image_attributes', 'twentysixteen_post_thumbnail_sizes_attr', 10, 3);

/**
 * Modifies tag cloud widget arguments to have all tags in the widget same font size.
 *
 * @since Twenty Sixteen 1.1
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array A new modified arguments.
 */
function twentysixteen_widget_tag_cloud_args($args) {
    $args['largest'] = 1;
    $args['smallest'] = 1;
    $args['unit'] = 'em';
    return $args;
}

add_filter('widget_tag_cloud_args', 'twentysixteen_widget_tag_cloud_args');


//custom code
define('FS_METHOD', 'direct');

//add custom js and css
function my_script() {
    wp_enqueue_script('jquery');
}

add_action('wp_enqueue_scripts', 'my_script', 0);


add_action('after_setup_theme', 'register_my_menu');

function register_my_menu() {
    register_nav_menu('footer', __('Footer Menu', 'theme-slug'));
}

//footer sidebar
//footer sidebar
add_action('widgets_init', 'theme_slug_widgets_init');

function theme_slug_widgets_init() {
    register_sidebar(array(
        'name' => __('Footer social box', 'theme-slug'),
        'id' => 'footer-social-box',
        'description' => __('Widgets in this area will be shown on all posts and pages.', 'theme-slug'),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
    /* register_sidebar( array(
      'name' => __( 'Footer logo', 'theme-slug' ),
      'id' => 'footer-logo',
      'description' => __( 'Widgets in this area will be shown on all posts and pages.', 'theme-slug' ),
      'before_widget' => '<li id="%1$s" class="widget %2$s">',
      'after_widget'  => '</li>',
      'before_title'  => '<h2 class="widgettitle">',
      'after_title'   => '</h2>',
      ) ); */
}

// Add a custom user role

$result = add_role('organizer', __(
                'Organizer'), array(
    'read' => true, // true allows this capability
    'edit_posts' => true, // Allows user to edit their own posts
    'edit_pages' => false, // Allows user to edit pages
    'edit_others_posts' => false, // Allows user to edit others posts not just their own
    'create_posts' => true, // Allows user to create new posts
    'manage_categories' => true, // Allows user to manage post categories
    'publish_posts' => true, // Allows the user to publish, otherwise posts stays in draft mode
    'edit_themes' => false, // false denies this capability. User can’t edit your theme
    'install_plugins' => false, // User cant add new plugins
    'update_plugin' => false, // User can’t update any plugins
    'update_core' => false // user cant perform core updates
        )
);

/**
 * Register a Cause post type. 
 */
add_action('init', 'codex_cause_init');

function codex_cause_init() {
    $labels = array(
        'name' => _x('Causes', 'post type general name', 'your-plugin-textdomain'),
        'singular_name' => _x('Cause', 'post type singular name', 'your-plugin-textdomain'),
        'menu_name' => _x('Causes', 'admin menu', 'your-plugin-textdomain'),
        'name_admin_bar' => _x('Cause', 'add new on admin bar', 'your-plugin-textdomain'),
        'add_new' => _x('Add New', 'cause', 'your-plugin-textdomain'),
        'add_new_item' => __('Add New Cause', 'your-plugin-textdomain'),
        'new_item' => __('New Cause', 'your-plugin-textdomain'),
        'edit_item' => __('Edit Cause', 'your-plugin-textdomain'),
        'view_item' => __('View Cause', 'your-plugin-textdomain'),
        'all_items' => __('All Cause', 'your-plugin-textdomain'),
        'search_items' => __('Search Causes', 'your-plugin-textdomain'),
        'parent_item_colon' => __('Parent Causes:', 'your-plugin-textdomain'),
        'not_found' => __('No events found.', 'your-plugin-textdomain'),
        'not_found_in_trash' => __('No events found in Trash.', 'your-plugin-textdomain')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'your-plugin-textdomain'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'cause'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'comments')
    );

    register_post_type('cause', $args);
}

/**
 * Register a event post type. 
 */
add_action('init', 'codex_event_init');

function codex_event_init() {
    $labels = array(
        'name' => _x('Events', 'post type general name', 'your-plugin-textdomain'),
        'singular_name' => _x('Event', 'post type singular name', 'your-plugin-textdomain'),
        'menu_name' => _x('Events', 'admin menu', 'your-plugin-textdomain'),
        'name_admin_bar' => _x('Event', 'add new on admin bar', 'your-plugin-textdomain'),
        'add_new' => _x('Add New', 'event', 'your-plugin-textdomain'),
        'add_new_item' => __('Add New Event', 'your-plugin-textdomain'),
        'new_item' => __('New Event', 'your-plugin-textdomain'),
        'edit_item' => __('Edit Event', 'your-plugin-textdomain'),
        'view_item' => __('View Event', 'your-plugin-textdomain'),
        'all_items' => __('All Event', 'your-plugin-textdomain'),
        'search_items' => __('Search Events', 'your-plugin-textdomain'),
        'parent_item_colon' => __('Parent Events:', 'your-plugin-textdomain'),
        'not_found' => __('No events found.', 'your-plugin-textdomain'),
        'not_found_in_trash' => __('No events found in Trash.', 'your-plugin-textdomain')
    );

    $args = array(
        'labels' => $labels,
        'description' => __('Description.', 'your-plugin-textdomain'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'event'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'comments')
    );

    register_post_type('event', $args);
}

function cause_category_taxonomies() {
    $labels = array(
        'name' => _x('Cause Category', 'taxonomy general name'),
        'singular_name' => _x('Cause Category Name', 'taxonomy singular name'),
        'search_items' => __('Search Cause Category Name'),
        'all_items' => __('All Cause Category Names '),
        'parent_item' => __('Parent Cause Category Name'),
        'parent_item_colon' => __('Parent Cause Category Name:'),
        'edit_item' => __('Edit Cause Category Name'),
        'update_item' => __('Update Cause Category Name'),
        'add_new_item' => __('Add Cause Category Name'),
        'new_item_name' => __('New Cause Category Name'),
        'not_found' => 'No Cause Category Name Found',
        'menu_name' => __('Cause Category Name'),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'show_admin_column' => true,
    );
    register_taxonomy('cause_category', array('cause', 'event'), $args);
}

add_action('init', 'cause_category_taxonomies', 0);

function state_category_taxonomies() {
    $labels = array(
        'name' => _x('State Category', 'taxonomy general name'),
        'singular_name' => _x('State Category Name', 'taxonomy singular name'),
        'search_items' => __('Search State Category Name'),
        'all_items' => __('All State Category Names '),
        'parent_item' => __('Parent State Category Name'),
        'parent_item_colon' => __('Parent State Category Name:'),
        'edit_item' => __('Edit State Category Name'),
        'update_item' => __('Update State Category Name'),
        'add_new_item' => __('Add State Category Name'),
        'new_item_name' => __('New State Category Name'),
        'not_found' => 'No State Category Name Found',
        'menu_name' => __('State Category Name'),
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'show_admin_column' => true,
    );
    register_taxonomy('state_category', array('cause', 'event'), $args);
}

add_action('init', 'state_category_taxonomies', 0);
/* add_action( 'init', 'create_event_tax' );

  function create_event_tax() {
  register_taxonomy(
  'cause_category',
  array(  'cause','event'),
  array(
  'label' => __( 'Cause Category' ),
  'rewrite' => array( 'slug' => 'cause_category' ),
  'hierarchical' => false,
  )
  );
  }
  add_action( 'init', 'create_state_tax' );

  function create_state_tax() {
  register_taxonomy(
  'state',
  array( 'cause','event'),
  array(
  'label' => __( 'State' ),
  'rewrite' => array( 'slug' => 'state' ),
  'hierarchical' => false,
  )
  );
  } */

function load_external_jQuery() {
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_register_script('my-script', get_template_directory_uri() . '/js/custom_join_cause.js', array('jquery'), time(), true);
    wp_enqueue_script('my-script');
    wp_register_script('register_user', get_template_directory_uri() . '/js/custom_register_user.js', array('jquery'), time(), true);

    wp_register_script('user_login', get_template_directory_uri() . '/js/custom_login.js', array('jquery'), time(), true);

    wp_register_script('share_by_email', get_template_directory_uri() . '/js/custom_share_by_email.js', array('jquery'), time(), true);

    wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
}

add_action('wp_enqueue_scripts', 'load_external_jQuery');

add_role(
        'individual', __('Individual'), array(
    'read' => true, // true allows this capability
    'edit_posts' => false,
    'delete_posts' => false, // Use false to explicitly deny
        )
);

add_role(
        'organization', __('Organization'), array(
    'read' => true, // true allows this capability
    'edit_posts' => false,
    'delete_posts' => false, // Use false to explicitly deny
        )
);

/* add_action( 'wp_ajax_nopriv_register_user', 'register_user' );
  add_action( 'wp_ajax_register_user', 'register_user' );
  function register_user() {
  return 'done';
  die();
  } */

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

function get_the_popular_excerpt($str_content, $word_count) {
    $excerpt = $str_content;
    $excerpt = preg_replace(" (\[.*?\])", '', $excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $word_count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace('/\s+/', ' ', $excerpt));
    $excerpt = $excerpt . ' [...]';
    return $excerpt;
}

function wordlimit($string, $length = 25) {
    $string = preg_replace(" (\[.*?\])", '', $string);
    $string = strip_shortcodes($string);
    $string = strip_tags($string);
    $string = trim(preg_replace('/\s+/', ' ', $string));
    $ellipsis = "...";
    $words = explode(' ', $string);
    if (count($words) > $length)
        return implode(' ', array_slice($words, 0, $length)) . $ellipsis;
    else
        return $string;
}

// Custom pagination code 
function pagination($wp_query, $display = true, $ajax = false, $p_cur = 1) {

    global $wp_rewrite;

    if ($ajax)
        $current = $p_cur;
    else
        $wp_query->query_vars['paged'] > 1 ? $current = $wp_query->query_vars['paged'] : $current = 1;

    $pagination = array(
        'base' => @add_query_arg('page2', '%#%'),
        'format' => '',
        'total' => $wp_query->max_num_pages,
        'current' => $current,
        'prev_text' => __('«'),
        'next_text' => __('»'),
        'type' => 'plain'
    );
    //if( $wp_rewrite->using_permalinks() )
    //$pagination['base'] = user_trinaailingslashit( trailingslashit( remove_query_arg( 's', get_pagenum_link( 1 ) ) ) . 'page/%#%/', 'paged' );

    if (!empty($wp_query->query_vars['s']))
        $pagination['add_args'] = array('s' => get_query_var('s'));

    if (!$display)
        return paginate_links($pagination);
    else
        return paginate_links($pagination);
}

add_action("wp_ajax_all_pagination", "all_pagination_fn");
add_action("wp_ajax_nopriv_all_pagination", "all_pagination_fn");

function all_pagination_fn() {
    $page = $_POST['page'];
    $type = $_POST['type'];
    echo "page" . $page;
    echo "type" . $type;
    $args = array(
        'posts_per_page' => 2,
        'offset' => $page * 2,
        'category' => '',
        'category_name' => '',
        'orderby' => 'date',
        'order' => 'DESC',
        'include' => '',
        'exclude' => '',
        'meta_key' => '',
        'meta_value' => '',
        'post_type' => array('event', 'cause'),
        'post_mime_type' => '',
        'post_parent' => '',
        'author' => '',
        'post_status' => 'publish',
        'suppress_filters' => true
    );
    $all_array = get_posts($args);
    print_r($all_array);
    //   foreach ($all_array as $all) {
    //    $html='<div style=" display: inline-block; opacity: 1;" class="portfolio all mix_all" data-cat="logo">';
    //    $html.='<div class="featured-causes-box"><a href="">'.get_the_post_thumbnail( $all->ID, "thumbnail" ).'</a>';
    //      $html.='<div class="shear"><a href="">like</a></div>';
    //      $html.='<div class="docoment-section">';
    //        $html.='<h2><a href="">'.$all->post_title.'</a></h2>';
    //        $html.='<p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>';
    //        $html.='<span class="graph"> <span class="pull-right">15%</span>';
    //        $html.='<div class="progress progress-warning active">';
    //          $html.='<div style="width: 15%;" class="bar"></div>';
    //        $html.='</div>';
    //        $html.='<strong>12 Days Left</strong> </span> </div>';
    //      $html.='<div class="grey-support">';
    //        $html.='<ul>';
    //          $html.='<li><span class="rupee"></span>';
    //            $html.='<p>Funded 80%</p>';
    //         $html.=' </li>';
    //          $html.='<li><span class="doller"></span>';
    //            $html.='<p>Pledged $2453</p>';
    //          $html.='</li>';
    //        $html.='</ul>';
    //        $html.='<div class="supprot"><a href="">Support</a></div>';
    //      $html.='</div>';
    //   $html.=' </div>';
    // $html.=' </div>';
    //  }
    // echo $html;
    exit;
}
