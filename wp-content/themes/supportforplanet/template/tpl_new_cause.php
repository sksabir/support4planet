<?php
/**
 * Template Name:Couse new template
 */
get_header();
?>
<style type="text/css">

    #loading {
        width: 100%;
        height: 100%;
        top: 0px;
        left: 0px;
        position: fixed;
        display: block;
        opacity: 0.7;
        background-color: #fff;
        z-index: 99;
        text-align: center;
    }

    #loading-image {
        position: absolute;
        top: 30%;
        left: 40%;
        z-index: 100;
    }
    .featured-causes-box img {
        height: 250px;
    }
    .no-data {
        clear: both;
    }
    .docoment-section {
        min-height: 150px;
        width: 100%;
    }
</style>
<?php $the_query = new WP_Query('page_id=9'); ?>
<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
    <?php the_content(); ?>
<?php endwhile; ?>
<div class="container featured">
    <div class="row">
        <div class="col-sm-7 col-md-7">
            <div class="col-sm-7 no-padding">
                <ul id="filters" class="clearfix">
                    <li><span class="filter active all_cause_tab" data-filter="all">All Causes </span></li>
                    <li><span class="filter whats_tending_tab" data-filter="whats-trending">What’s Trending</span></li>
                    <li><span class="filter latest_tab" data-filter="latest">Latest</span></li>
                </ul>
            </div>
            <div class="col-sm-5 no-padding">
                <div class="select-one">
                    <?php
                    $taxonomy = 'cause_category';
                    $queried_term = get_term_by('slug', get_query_var($taxonomy));
                    $terms = get_terms($taxonomy);
                    ?>
                    <p>Categories:</p>
                    <label>
                        <select name="cat" id="cat">
                            <option value="" selected>Select Category</option>
                            <?php foreach ($terms as $value) { ?>
                                <option value="<?php echo $value->term_id; ?>"><?php echo $value->name; ?></option>
                            <?php } ?>     
                        </select>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-sm-5 col-md-5">
            <div class="map-direction">
                <p>Find events near you:</p>
                <form>
                    <div class="input-group">
                        <input type="text" id="zipcode" class="form-control input-search" placeholder="Enter your zipcode">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-primary" id="go">Go</button>
                        </span> </div>
                </form>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 view-cell">
            <div id="portfoliolist">
                <?php
                //alll  
                $args = array(
                    'posts_per_page' => 3,
                    'offset' => 0,
                    'category' => '',
                    'category_name' => '',
                    'orderby' => 'date',
                    'order' => 'DESC',
                    'include' => '',
                    'exclude' => '',
                    'meta_key' => '',
                    'meta_value' => '',
                    'post_type' => array('event', 'cause'),
                    'post_mime_type' => '',
                    'post_parent' => '',
                    'author' => '',
                    'post_status' => 'publish',
                    'suppress_filters' => true
                );
                $all_array = get_posts($args);
                // echo "aaa".count($all_array);
                // die;
                $clases = '';
                foreach ($all_array as $all) {
                    $term_list = wp_get_post_terms($all->ID, 'cause_category', array("fields" => "slugs"));
                    if (count($term_list) > 0) {
                        $clases = implode(" ", $term_list);
                    }
                    ?>
                    <div style=" display: inline-block; opacity: 1;" class="portfolio all <?php echo $clases; ?> latest" data-cat="logo">
                        <div class="featured-causes-box"><a href=""><?php
                                if (get_the_post_thumbnail($all->ID, 'thumbnail') == "") {
                                    ?>
                                    <img src="<?php echo site_url(); ?>/wp-content/uploads/2016/04/placeholder.png" alt=""/>
                                    <?php
                                } else {
                                    echo get_the_post_thumbnail($all->ID, 'thumbnail');
                                }
                                ?>
                            </a>
                            <div class="shear"><a href="">like</a></div>
                            <div class="docoment-section">
                                <h2><a href=""><?php echo $all->post_title; ?></a></h2>
                                <p><?php
                                    if (strlen($all->post_content) > 100) {
                                        echo substr(strip_tags($all->post_content), 0, 100) . "...";
                                    } else {
                                        echo substr(strip_tags($all->post_content), 0, 100);
                                    }
                                    ?></p>
                                <span class="graph"> <span class="pull-right">15%</span>
                                    <div class="progress progress-warning active">
                                        <div style="width: 15%;" class="bar"></div>
                                    </div>
                                    <strong>12 Days Left</strong> </span> </div>
                            <div class="grey-support">
                                <ul>
                                    <li><span class="rupee"></span>
                                        <p>Funded 80%</p>
                                    </li>
                                    <li><span class="doller"></span>
                                        <p>Pledged $2453</p>
                                    </li>
                                </ul>
                                <div class="supprot"><a href="">Support</a></div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>
            <div class="all-view-causes"><a  class="view-causes all-pagi" id="all-pagi" page="1" type="">View More Causes</a> </div>
        </div>
    </div>
    <!-- Featured end --> 
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery("#go").click(function () {
                var zipcode = jQuery("#zipcode").val();
                if (zipcode == '')
                {
                    alert("Please enter zipcode");
                } else {
                    jQuery.ajax({
                        url: 'http://c04.481.myftpupload.com/wp-admin/admin-ajax.php',
                        type: 'post',
                        data: {action: 'zipcode_search', zipcode: zipcode},
                        beforeSend: function () {
                            jQuery("#loading").show();
                        },
                        success: function (response) {
                            jQuery("#loading").hide();
                            jQuery("#portfoliolist").html(response);
                            jQuery("#zipcode").val('');

                        }
                    });
                }

            })


            jQuery('#cat').change(function () {
                var cat = jQuery(this).val();
                var page = jQuery("#all-pagi").attr("page");
                var type = jQuery("#all-pagi").attr("type");
                var nextpage = parseInt(page) + 1;
                jQuery(".all-view-causes").show();
                if (cat == '')
                {
                    location.reload();
                } else {
                    jQuery.ajax({
                        url: 'http://c04.481.myftpupload.com/wp-admin/admin-ajax.php',
                        type: 'post',
                        data: {action: 'cat_search', cat: cat},
                        beforeSend: function () {
                            jQuery("#loading").show();
                        },
                        success: function (response) {
                            jQuery("#loading").hide();
                            if (cat == '')
                            {

                            } else {
                                jQuery("#portfoliolist").html(response);
                                jQuery("#all-pagi").removeAttr("page");
                                jQuery("#all-pagi").attr("page", "1");
                                jQuery("#all-pagi").attr("type", cat);
                            }


                        }
                    });
                }
            });

            jQuery(document).on("click", "#all-pagi", function (e) {
                e.preventDefault();
                var page = jQuery(this).attr("page");
                var type = jQuery(this).attr("type");
                var nextpage = parseInt(page) + 1;
                jQuery.ajax({
                    url: 'http://c04.481.myftpupload.com/wp-admin/admin-ajax.php',
                    type: 'post',
                    data: {action: 'all_pagination', page: page, type: type},
                    beforeSend: function () {
                        jQuery("#loading").show();
                    },
                    success: function (response) {
                        jQuery("#loading").hide();
                        if (response == '')
                        {
                            jQuery(".all-view-causes").hide();
                            jQuery("#portfoliolist").append('<div class="no-data">No more data</div>');
                        } else {
                            jQuery("#portfoliolist").append(response);
                            jQuery("#all-pagi").removeAttr("page");
                            jQuery("#all-pagi").attr("page", nextpage);
                        }


                    }
                });
            });
        })
    </script>
    <div id="loading"   style="display:none">
        <img id="loading-image" src="<?php echo get_template_directory_uri(); ?>/images/Loader.gif" alt="Loading..."/>
    </div>
    <?php get_footer(); ?>