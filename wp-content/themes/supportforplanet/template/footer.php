<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-md-offset-3 footer-contaned">
                <?php dynamic_sidebar('footer-social-box'); ?>
                <div class="footer-menu">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'footer'
                    ));
                    ?>
                </div>
                <?php dynamic_sidebar('footer-logo'); ?>
               <!--  <div class="footer-logo"><img src="<?php echo get_template_directory_uri(); ?>/images/footer_logo.png"  alt=""/></div>
                <p>© 2016 Support For Planet. All rights reserved.</p> -->
            </div>
        </div>
    </div>
</footer>
<!-- Footer end --> 
<!-- Js --> 
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script> 
<script src="<?php echo get_template_directory_uri(); ?>/js/custom.js"></script> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.mixitup.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/easyResponsiveTabs.js"></script>  


<!-- Js End -->
<?php wp_footer(); ?>
</body>
</html>
