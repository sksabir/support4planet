<?php
/**
 * Template Name:Edit Profile template
 */
if (!is_user_logged_in()) {
    ?>
    <script type="text/javascript">window.location.href = "<?php echo site_url(); ?>/log-in/";</script>
    <?php
}
get_header();
wp_reset_query();
$user_ID = get_current_user_id();

if (isset($_POST['org_name'])) {
    update_field('field_56de7e95563cd', $_POST['org_name'], 'user_' . $user_ID);
}
if (isset($_POST['phone'])) {
    update_field('field_56de7e139d862', $_POST['phone'], 'user_' . $user_ID);
}

if (isset($_POST['street_address'])) {
    update_field('field_56efa9bb2ed06', $_POST['street_address'], 'user_' . $user_ID);
}
if (isset($_POST['city'])) {
    update_field('field_56efa9d92ed07', $_POST['city'], 'user_' . $user_ID);
}
if (isset($_POST['state'])) {
    update_field('field_56efa9e52ed08', $_POST['state'], 'user_' . $user_ID);
}
if (isset($_POST['zip'])) {
    update_field('field_56efa9f42ed09', $_POST['zip'], 'user_' . $user_ID);
}

if (isset($_POST['fName'])) {
    update_user_meta($user_ID, 'first_name', $_POST['fName']);
}
if (isset($_POST['lName'])) {
    update_user_meta($user_ID, 'last_name', $_POST['lName']);
}
if (isset($_POST['bio'])) {
    update_user_meta($user_ID, 'description', $_POST['bio']);
}

if ($_FILES['profile_image']['name'] != '') {

    // These files need to be included as dependencies when on the front end.
    require_once( ABSPATH . 'wp-admin/includes/image.php' );
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
    require_once( ABSPATH . 'wp-admin/includes/media.php' );

    // Let WordPress handle the upload.
    // Remember, 'my_image_upload' is the name of our file input in our form above.
    $attachment_id = media_handle_upload('profile_image', 0);
    //echo '==========';

    if (is_wp_error($attachment_id)) {
        //echo 'There was an error uploading the image.';
    } else {
        //echo 'The image was uploaded successfully!';

        $val = update_field('field_56efa99a2ed04', $attachment_id, 'user_' . $user_ID);
        //print_r($val);
    }
}


$fName = get_user_meta($user_ID, 'first_name', true);
$lName = get_user_meta($user_ID, 'last_name', true);
$bio = get_user_meta($user_ID, 'description', true);
$org_name = get_field('organization_name', 'user_' . $user_ID);
$phone = get_field('phone_number', 'user_' . $user_ID);

$street_address = get_field('street_address', 'user_' . $user_ID);
$city = get_field('city', 'user_' . $user_ID);
$state = get_field('state', 'user_' . $user_ID);
$zip = get_field('zip', 'user_' . $user_ID);

$user = get_userdata($user_ID);

$roles = implode(', ', $user->roles);
//print_r($user);


$pic = get_field('profile_picture', 'user_' . $user_ID);
if (!empty($pic)) {
    $profile_picture = get_field('profile_picture', 'user_' . $user_ID);
    $url = $profile_picture['url'];
    $alt = $profile_picture['alt'];
} else {
    $url = get_template_directory_uri() . '/images/blank.jpg';
    $alt = '';
}
?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-select.css">
<style type="text/css">
    .error-msg {
        border: 1px solid red !important;
    }
    .error {
        color: red;
    }
</style>
<div class="container">
    <div class="row">
        <div class="causeFormWrap">
            <h2>Edit Profile</h2>

            <div class="editFormHolder">
                <div class="cause-form">
                    <div class="innerGap">

                        <form class="form-horizontal causeForm" method="post" enctype="multipart/form-data" action=""> 
                            <div class="col-sm-4">
                                <!--<div class="imgUpload">
                                  <a href="#">-->
                                <span class="transparentBg"></span>
                                <!--<span class="editinfo">
                                    Edit Profile Picture
                                </span>-->
                                <?php //echo do_shortcode('[avatar_upload]');  ?>
                                <img style="float:none;margin: 0 auto;border-radius: 100%;" class="img-responsive" src="<?php echo $url; ?>" alt="<?php echo $alt; ?>" />
                                <!--</a>
                                    </div>-->
                                <!--<a href="#" class="link">Remove Picture</a>-->
                            </div>
                            <div class="col-sm-8">
                                <?php
                                if ($roles == 'organization') {
                                    ?>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Organization Name:</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" name="org_name" type="text" value="<?php echo $org_name; ?>" placeholder="Enter the charity name">
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="form-group">       
                                    <label  class="col-sm-4 control-label">Enter Name:</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="text" placeholder="First Name" name="fName" value="<?php echo $fName; ?>">
                                    </div>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="text" placeholder="Last Name" name="lName" value="<?php echo $lName; ?>">
                                    </div>

                                </div>

                                <div class="form-group">       
                                    <label  class="col-sm-4 control-label">Postal Address:</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" name="street_address" value="<?php echo $street_address; ?>" placeholder="Enter the Street address">
                                        <input type="text" class="form-control" name="city" value="<?php echo $city; ?>" placeholder="Enter the name of the City">
                                        <input type="text" class="form-control" name="state" value="<?php echo $state; ?>" placeholder="Enter State">
                                        <!-- <select class="form-control selectpicker">
                                          <option>Choose State</option>
                                          <option>Choose State</option>
                                          <option>Choose State</option>
                                        </select> -->

                                        <input type="text" class="form-control" name="zip" value="<?php echo $zip; ?>" placeholder="Enter Zipcode">
                                    </div>
                                </div>

                                <div class="form-group">       
                                    <label  class="col-sm-4 control-label">Additional Info:</label>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="phone" value="<?php echo $phone; ?>" placeholder="Enter Phone No.">
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" value="<?php echo $user->user_email; ?>" readonly="" placeholder="Email Id">
                                    </div>

                                </div>

                                <div class="form-group">       
                                    <label  class="col-sm-4 control-label">About Me:</label>
                                    <div class="col-sm-8">
                                        <textarea placeholder="Enter full bio" name="bio" class="form-control"><?php echo $bio; ?></textarea>
                                    </div>

                                </div>
                                <div class="form-group">       
                                    <label  class="col-sm-4 control-label">Upload Image:</label>
                                    <div class="col-sm-8">
                                    <!-- <div class="flile-control"><input type="file" name="file_cause" id="file_cause" class="form-control"></div> -->
                                        <div class="flile-control">
                                            <input type="file" name="profile_image" id="file_cause" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" />
                                            <label for="file_cause"><img src="<?php echo site_url(); ?>/wp-content/themes/saveforplanet/images/imgIcon.png" alt=""> <span>Browse Images From Computer</span></label>
                                        </div>
                                    </div>                    
                                </div>
                                <div class="form-group">       
                                    <label  class="col-sm-4 control-label"></label>
                                    <div class="col-sm-8">
                                        <div id="dvPreview"></div>
                                    </div>
                                </div>
                            </div>

                            <footer class="formFoot">

                                <input type="submit" value="Update Profile">
                            </footer>

                        </form>



                    </div> 
                </div>

            </div>
        </div>
    </div>
</div>
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-select.js"></script>
<script>
        jQuery(document).ready(function (e) {
            jQuery(".changeBtn").click(function (t) {
                t.preventDefault();
                jQuery(".passwordDetailBox").slideToggle();
            });
        });
</script>
<?php
//wp_enqueue_script( 'user_login' );
get_footer();
?>