<?php
/**
 * Template Name:home template
 */
get_header();
?>


<!-- banner -->
<div class="container-fluid banner-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1 banner-box">
                <h2><?php echo get_field('title_cause', get_the_ID()); ?></h2>
                <p><?php echo get_field('description_cause', get_the_ID()); ?></p>
                <a href="<?php echo get_field('button_level_cause_link', get_the_ID()); ?>" class="join-cause"><?php echo get_field('button_level_cause', get_the_ID()); ?></a>
                <div class="scroll-Down">
                    <p>Scroll Down</p>
                    <span></span></div>
            </div>
        </div>
    </div>
</div>
<!-- banner end--> 


<!-- About -->
<div class="container about-section">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2 about-inner">
            <h2><?php echo get_field('title_about', get_the_ID()); ?></h2>
            <p><?php echo get_field('about_description', get_the_ID()); ?></p>
            <a href="<?php echo get_field('button_level_about_link', get_the_ID()); ?>" class="know-more"><?php echo get_field('button_level_about', get_the_ID()); ?></a> 
        </div>
    </div>
</div>
<!-- About end --> 

<!-- our-vertion -->
<div class="container-fluid our-vertion">
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-md-4 col-lg-4">
                <div class="our-joinus"> <span class="icon-miss"></span>
                    <h2><?php echo get_field('title1', get_the_ID()); ?></h2>
                    <?php echo get_field('description1', get_the_ID()); ?>
                </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
                <div class="our-joinus"> <span class="icon-vis "></span>
                    <h2><?php echo get_field('title2', get_the_ID()); ?></h2>
                    <?php echo get_field('description2', get_the_ID()); ?>
                    <a class="join-cause" href="<?php echo get_field('our_mission_button_level_link', get_the_ID()); ?>"><?php echo get_field('our_mission_button_level', get_the_ID()); ?></a> </div>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
                <div class="our-joinus"> <span class="icon-promis"></span>
                    <h2><?php echo get_field('title3', get_the_ID()); ?></h2>
                    <?php echo get_field('description3', get_the_ID()); ?>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- our-vertion end --> 
<!-- post slider -->
<div class="container-fluid featured">
    <div class="container">
        <div class="row">
            <div class="slider">
                <h2><?php echo get_field('featured_causes_title', get_the_ID()); ?></h2>
                <div class="c_logo">
                    <div class="item">
                        <div class="featured-causes-box"><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/post-7.jpg"  alt=""/></a>
                            <div class="shear"><a href="">like</a></div>
                            <div class="docoment-section">
                                <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                                <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                                <span class="graph"> <span class="pull-right">15%</span>
                                    <div class="progress progress-warning active">
                                        <div class="bar" style="width: 15%;"></div>
                                    </div>
                                    <strong>12 Days Left</strong> </span> </div>
                            <div class="grey-support">
                                <ul>
                                    <li><span class="rupee"></span>
                                        <p>Funded 80%</p>
                                    </li>
                                    <li><span class="doller"></span>
                                        <p>Pledged $2453</p>
                                    </li>
                                </ul>
                                <div class="supprot"><a href="">Support</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-causes-box"><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/post-8.jpg"  alt=""/></a>
                            <div class="shear"><a href="">like</a></div>
                            <div class="docoment-section">
                                <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                                <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                                <span class="graph"> <span class="pull-right">15%</span>
                                    <div class="progress progress-warning active">
                                        <div class="bar" style="width: 15%;"></div>
                                    </div>
                                    <strong>12 Days Left</strong> </span> </div>
                            <div class="grey-support">
                                <ul>
                                    <li><span class="rupee"></span>
                                        <p>Funded 80%</p>
                                    </li>
                                    <li><span class="doller"></span>
                                        <p>Pledged $2453</p>
                                    </li>
                                </ul>
                                <div class="supprot"><a href="">Support</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-causes-box"><a href=""> <img src="<?php echo get_template_directory_uri(); ?>/images/post-9.jpg"  alt=""/></a>
                            <div class="shear"><a href="">like</a></div>
                            <div class="docoment-section">
                                <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                                <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                                <span class="graph"> <span class="pull-right">15%</span>
                                    <div class="progress progress-warning active">
                                        <div class="bar" style="width: 15%;"></div>
                                    </div>
                                    <strong>12 Days Left</strong> </span> </div>
                            <div class="grey-support">
                                <ul>
                                    <li><span class="rupee"></span>
                                        <p>Funded 80%</p>
                                    </li>
                                    <li><span class="doller"></span>
                                        <p>Pledged $2453</p>
                                    </li>
                                </ul>
                                <div class="supprot"><a href="">Support</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-causes-box"> <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/post-1.jpg"  alt=""/></a>
                            <div class="shear"><a href="">like</a></div>
                            <div class="docoment-section">
                                <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                                <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                                <span class="graph"> <span class="pull-right">15%</span>
                                    <div class="progress progress-warning active">
                                        <div class="bar" style="width: 15%;"></div>
                                    </div>
                                    <strong>12 Days Left</strong> </span> </div>
                            <div class="grey-support">
                                <ul>
                                    <li><span class="rupee"></span>
                                        <p>Funded 80%</p>
                                    </li>
                                    <li><span class="doller"></span>
                                        <p>Pledged $2453</p>
                                    </li>
                                </ul>
                                <div class="supprot"><a href="">Support</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-causes-box"><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/post-2.jpg"  alt=""/></a>
                            <div class="shear"><a href="">like</a></div>
                            <div class="docoment-section">
                                <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                                <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                                <span class="graph"> <span class="pull-right">15%</span>
                                    <div class="progress progress-warning active">
                                        <div class="bar" style="width: 15%;"></div>
                                    </div>
                                    <strong>12 Days Left</strong> </span> </div>
                            <div class="grey-support">
                                <ul>
                                    <li><span class="rupee"></span>
                                        <p>Funded 80%</p>
                                    </li>
                                    <li><span class="doller"></span>
                                        <p>Pledged $2453</p>
                                    </li>
                                </ul>
                                <div class="supprot"><a href="">Support</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-causes-box"><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/post-2.jpg"  alt=""/></a>
                            <div class="shear"><a href="">like</a></div>
                            <div class="docoment-section">
                                <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                                <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                                <span class="graph"> <span class="pull-right">15%</span>
                                    <div class="progress progress-warning active">
                                        <div class="bar" style="width: 15%;"></div>
                                    </div>
                                    <strong>12 Days Left</strong> </span> </div>
                            <div class="grey-support">
                                <ul>
                                    <li><span class="rupee"></span>
                                        <p>Funded 80%</p>
                                    </li>
                                    <li><span class="doller"></span>
                                        <p>Pledged $2453</p>
                                    </li>
                                </ul>
                                <div class="supprot"><a href="">Support</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-causes-box"><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/post-3.jpg"  alt=""/></a>
                            <div class="shear"><a href="">like</a></div>
                            <div class="docoment-section">
                                <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                                <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                                <span class="graph"> <span class="pull-right">15%</span>
                                    <div class="progress progress-warning active">
                                        <div class="bar" style="width: 15%;"></div>
                                    </div>
                                    <strong>12 Days Left</strong> </span> </div>
                            <div class="grey-support">
                                <ul>
                                    <li><span class="rupee"></span>
                                        <p>Funded 80%</p>
                                    </li>
                                    <li><span class="doller"></span>
                                        <p>Pledged $2453</p>
                                    </li>
                                </ul>
                                <div class="supprot"><a href="">Support</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-causes-box"><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/post-4.jpg"  alt=""/></a>
                            <div class="shear"><a href="">like</a></div>
                            <div class="docoment-section">
                                <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                                <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                                <span class="graph"> <span class="pull-right">15%</span>
                                    <div class="progress progress-warning active">
                                        <div class="bar" style="width: 15%;"></div>
                                    </div>
                                    <strong>12 Days Left</strong> </span> </div>
                            <div class="grey-support">
                                <ul>
                                    <li><span class="rupee"></span>
                                        <p>Funded 80%</p>
                                    </li>
                                    <li><span class="doller"></span>
                                        <p>Pledged $2453</p>
                                    </li>
                                </ul>
                                <div class="supprot"><a href="">Support</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="featured-causes-box"><a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/post-5.jpg"  alt=""/></a>
                            <div class="shear"><a href="">like</a></div>
                            <div class="docoment-section">
                                <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                                <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                                <span class="graph"> <span class="pull-right">15%</span>
                                    <div class="progress progress-warning active">
                                        <div class="bar" style="width: 15%;"></div>
                                    </div>
                                    <strong>12 Days Left</strong> </span> </div>
                            <div class="grey-support">
                                <ul>
                                    <li><span class="rupee"></span>
                                        <p>Funded 80%</p>
                                    </li>
                                    <li><span class="doller"></span>
                                        <p>Pledged $2453</p>
                                    </li>
                                </ul>
                                <div class="supprot"><a href="">Support</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="blue-involved">
        <div class="row">
            <div class="col-sm-4"><img src="<?php echo get_field('image_get_involved', get_the_ID()); ?>"  alt=""/></div>
            <div class="col-sm-8">
                <h2><?php echo get_field('title_get_involved', get_the_ID()); ?></h2>
                <?php echo get_field('description_get_involved', get_the_ID()); ?>
                <ul>
                    <li><a href="<?php echo get_field('button_level_volunteer_link', get_the_ID()); ?>"><?php echo get_field('button_level_volunteer', get_the_ID()); ?></a></li>
                    <li><a href="<?php echo get_field('button_level_non_profit_link', get_the_ID()); ?>"><?php echo get_field('button_level_non_profit', get_the_ID()); ?></a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1 help-section">
                <h2><?php echo get_field('how_you_can_help_title', get_the_ID()); ?></h2>
                <div class="col-sm-4 together"> <span class="helpothers"></span>
                    <p><?php echo get_field('how_you_can_help_title1', get_the_ID()); ?></p>
                </div>
                <div class="col-sm-4 together2"> <span class="cause"></span>
                    <p> <?php echo get_field('how_you_can_help_title2', get_the_ID()); ?></p>
                </div>
                <div class="col-sm-4 together"> <span class="support"></span>
                    <p><?php echo get_field('how_you_can_help_title3', get_the_ID()); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- post slider end --> 

<!-- Logo Add -->
<div class="container-fluid logo-section">
    <div class="container">
        <div class="clientlogo" style="color:#000;">
            <?php if (have_rows('logo_slider_images')): ?>
                <?php while (have_rows('logo_slider_images')): the_row(); ?>
                    <div class="item">
                        <?php $image = get_sub_field('image_logo'); ?>
                        <div class="sm-logo"><img src="<?php echo $image['url']; ?>"  alt=""/></div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>      
        </div>
    </div>
</div>
<!-- Logo Add End --> 
<?php get_footer(); ?>