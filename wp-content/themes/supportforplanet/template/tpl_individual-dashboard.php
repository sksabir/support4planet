<?php
/**
 * Template Name:Individual Dashboard template
 */
if (!is_user_logged_in()) {
    ?>
    <script type="text/javascript">window.location.href = "<?php echo site_url(); ?>/log-in/";</script>
    <?php
} else {
    $user_ID = get_current_user_id();
    $user = get_userdata($user_ID);
    $fName = get_user_meta($user_ID, 'first_name', true);
    $lName = get_user_meta($user_ID, 'last_name', true);

    $pic = get_field('profile_picture', 'user_' . $user_ID);
    if (!empty($pic)) {
        $profile_picture = get_field('profile_picture', 'user_' . $user_ID);
        $url = $profile_picture['url'];
        $alt = $fName . ' ' . $lName;
    } else {
        $url = get_template_directory_uri() . '/images/blank.jpg';
        $alt = $fName . ' ' . $lName;
    }


    $bio = get_user_meta($user_ID, 'description', true);
    $phone = get_field('phone_number', 'user_' . $user_ID);

    $street_address = get_field('street_address', 'user_' . $user_ID);
    $city = get_field('city', 'user_' . $user_ID);
    $state = get_field('state', 'user_' . $user_ID);
    $zip = get_field('zip', 'user_' . $user_ID);
    $roles = implode(', ', $user->roles);
    //print_r($user);
    if ($roles != 'individual') {
        ?>
        <script type="text/javascript">window.location.href = "<?php echo site_url(); ?>";</script>
        <?php
    }
}
get_header();
?>
<section class="profileDetailSec">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="imgUpload">
                    <img alt="<?php echo $alt; ?>" src="<?php echo $url; ?>">
                </div>
            </div>
            <div class="col-md-6">
                <div class="profileDes">
                    <h3><?php echo $fName . ' ' . $lName; ?></h3>
                    <ul>
                        <li><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/googleIcon.png"> <?php echo $city . ', ' . $state; ?></li>
                        <li>Joined on <?php echo date('M j, Y', strtotime($user->user_registered)); ?> </li>
                        <?php
                        $donation_qry = "SELECT COUNT(DISTINCT `post_id`) FROM {$wpdb->prefix}donation WHERE `user_id`='" . $user_ID . "' AND `status`='paid'";
                        $query_run = $wpdb->get_results($donation_qry, OBJECT);
                        $res = "COUNT(DISTINCT `post_id`)";
//echo '<pre>';
//print_r($query_run[0]->$res);
//echo '</pre>';
                        ?>
                        <li><strong><?php echo $query_run[0]->$res; ?></strong> Causes Supported</li>
                        <?php
                        $donation_qry = "SELECT COUNT(DISTINCT `post_id`) FROM {$wpdb->prefix}single_post_rating WHERE `user_id`='" . $user_ID . "'";
                        $query_run = $wpdb->get_results($donation_qry, OBJECT);
                        $res = "COUNT(DISTINCT `post_id`)";
                        //echo '<pre>';
                        //print_r($query_run[0]->$res);
                        //echo '</pre>';
                        ?>
                        <li><strong><?php echo $query_run[0]->$res; ?></strong> Causes Liked</li>
                    </ul>
                    <p><?php echo wordlimit($bio, 50); ?></p>

                    <a data-target=".bs-example-modal-sm" data-toggle="modal" class="seeMore" href="#">See Full Bio</a>

                    <div aria-labelledby="mySmallModalLabel" role="dialog" tabindex="-1" class="modal fade bs-example-modal-sm">
                        <div class="modal-dialog donateModal fullBioModal">
                            <div class="modal-content outLine">
                                <div class="innerBox form-horizontal innerPadding">
                                    <button aria-label="Close" data-dismiss="modal" class="close" type="button">
                                        <span aria-hidden="true"></span>
                                    </button>
                                    <h2>Full Bio</h2>
                                    <?php echo $bio; ?>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <div class="col-md-3">
                <div class="rightBox">
                    <h4>Funds Donated Till Now</h4>
                    <?php
                    $amount_qry = "SELECT SUM(`amount`) FROM {$wpdb->prefix}donation WHERE `user_id`='" . $user_ID . "' AND `status`='paid'";
                    $query_run = $wpdb->get_results($amount_qry, OBJECT);
                    $res = "SUM(`amount`)";
//echo '<pre>';
//print_r($query_run[0]->$res);
//echo '</pre>';
                    ?>
                    <p><img alt="" src="<?php echo get_template_directory_uri(); ?>/images/life_s_icon.png"> $<?php echo number_format($query_run[0]->$res); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>