<?php
/**
 * Template Name:Join Cause template
 */
get_header();
?>
<style type="text/css">
    .error-msg {
        border: 1px solid red !important;
    }
    .error {
        color: red;
    }
    #dvPreview, #dvPreview1{
        text-align: center;
    }
    .formFoot input[type="button"] {
        background: #fff44d none repeat scroll 0 0;
        border: medium none;
        border-radius: 33px;
        color: #004a83;
        display: inline-block;
        font-size: 16px;
        font-weight: 700;
        margin-left: 18px;
        margin-top: 0;
        padding: 19px 38px;
    }
</style>

<?php
/* echo '<pre>';
  print_r($_POST);
  echo '</pre>'; */


if (isset($_POST['join_cause'])) {

    $cause_success_msg = '';


    extract($_POST);


    $post = array(
        'post_title' => wp_strip_all_tags($cause_name),
        'post_content' => $additional_info,
        'post_status' => 'publish', // Choose: publish, preview, future, etc.
        'post_type' => 'cause'  // Use a custom post type if you want to
    );

    $insert_post_id = wp_insert_post($post);


    if ($insert_post_id != '') { // check post id 
        // Image Upload
        if ($_FILES['file_cause']['name'] != '') {

            // These files need to be included as dependencies when on the front end.
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );

            // Let WordPress handle the upload.
            // Remember, 'my_image_upload' is the name of our file input in our form above.
            $attachment_id = media_handle_upload('file_cause', $insert_post_id);
            //echo '==========';

            if (is_wp_error($attachment_id)) {
                echo 'There was an error uploading the image.';
            } else {
                echo 'The image was uploaded successfully!';

                set_post_thumbnail($insert_post_id, $attachment_id);
            }
        } else {

            // The security check failed, maybe show the user an error.
        }



        if (!empty($cause_state)) {

            wp_set_object_terms($insert_post_id, $cause_state, 'state_category');
        }

        if (!empty($category_of_the_cause)) {
            wp_set_object_terms($insert_post_id, $category_of_the_cause, 'cause_category');
        }


        if (!empty($cause_organization_name)) {
            add_post_meta($insert_post_id, 'organization_name', $cause_organization_name, true)or
                    update_post_meta($insert_post_id, 'organization_name', $cause_organization_name, true)
            ;
        }


        if (!empty($cause_contact_name)) {
            add_post_meta($insert_post_id, 'contact_name', $cause_contact_name, true)or
                    update_post_meta($insert_post_id, 'contact_name', $cause_contact_name, true);
        }

        if (!empty($cause_contact_title)) {
            add_post_meta($insert_post_id, 'contact_title', $cause_contact_title, true)or
                    update_post_meta($insert_post_id, 'contact_title', $cause_contact_title, true);
        }


        if (!empty($cause_street_address)) {
            add_post_meta($insert_post_id, 'street_address', $cause_street_address, true)or
                    update_post_meta($insert_post_id, 'street_address', $cause_street_address, true);
        }


        if (!empty($cause_city)) {
            add_post_meta($insert_post_id, 'city', $cause_city, true)or
                    update_post_meta($insert_post_id, 'city', $cause_city, true);
        }


        if (!empty($cause_zipcode)) {
            add_post_meta($insert_post_id, 'zipcode', $cause_zipcode, true)or
                    update_post_meta($insert_post_id, 'zipcode', $cause_zipcode, true);
        }


        if (!empty($cause_phone_no)) {
            add_post_meta($insert_post_id, 'phone_no', $cause_phone_no, true)or
                    update_post_meta($insert_post_id, 'phone_no', $cause_phone_no, true);
        }

        if (!empty($cause_email_id)) {
            add_post_meta($insert_post_id, 'email_id', $cause_email_id, true)or
                    update_post_meta($insert_post_id, 'email_id', $cause_email_id, true);
        }


        if (!empty($cause_url)) {
            add_post_meta($insert_post_id, 'url', $cause_url, true)or
                    update_post_meta($insert_post_id, 'url', $cause_url, true);
        }


        if (!empty($enter_tags)) {
            add_post_meta($insert_post_id, 'enter_tags', $enter_tags, true)or
                    update_post_meta($insert_post_id, 'enter_tags', $enter_tags, true);
        }

        if (!empty($end_cause_date)) {
            add_post_meta($insert_post_id, 'end_date_time', $end_cause_date, true)or
                    update_post_meta($insert_post_id, 'end_date_time', $end_cause_date, true);
        }

        if (!empty($type_of_cause)) {
            add_post_meta($insert_post_id, 'type_of_cause', $type_of_cause, true)or
                    update_post_meta($insert_post_id, 'type_of_cause', $type_of_cause, true);
        }

        if (!empty($tax_id)) {
            add_post_meta($insert_post_id, 'tax_id', $tax_id, true)or
                    update_post_meta($insert_post_id, 'tax_id', $tax_id, true);
        }


        if (!empty($fundraising_goal)) {
            add_post_meta($insert_post_id, 'fundraising_goal', $fundraising_goal, true)or
                    update_post_meta($insert_post_id, 'fundraising_goal', $fundraising_goal, true);
        }


        $cause_success_msg = 'Cause post added successfully';
    } else {

        $cause_success_msg = 'Cause post not added successfully';
    }
}



if (isset($_POST['event_name'])) {

    $event_success_msg = '';

    extract($_POST);

    $post_event = array(
        'post_title' => wp_strip_all_tags($event_name),
        'post_status' => 'publish', // Choose: publish, preview, future, etc.
        'post_type' => 'event', // Use a custom post type if you want to
        'post_content' => $additional_info_event
    );

    $insert_event_post_id = wp_insert_post($post_event);


    if ($insert_event_post_id != '') {
        // Image Upload
        if ($_FILES['file_event']['name'] != '') {

            // These files need to be included as dependencies when on the front end.
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            require_once( ABSPATH . 'wp-admin/includes/file.php' );
            require_once( ABSPATH . 'wp-admin/includes/media.php' );

            $attachment_id = media_handle_upload('file_event', $insert_event_post_id);
            //echo '==========';

            if (is_wp_error($attachment_id)) {
                echo 'There was an error uploading the image.';
            } else {
                echo 'The image was uploaded successfully!';
                set_post_thumbnail($insert_event_post_id, $attachment_id);
            }
        }


        if (!empty($event_state)) {
            wp_set_object_terms($insert_event_post_id, $event_state, 'state_category');
        }

        if (!empty($category_of_the_event)) {
            wp_set_object_terms($insert_event_post_id, $category_of_the_event, 'cause_category');
        }


        if (!empty($event_organization_name)) {
            add_post_meta($insert_event_post_id, 'organization_name', $event_organization_name, true)or
                    update_post_meta($insert_event_post_id, 'organization_name', $event_organization_name, true);
        }

        if (!empty($event_contact_name)) {
            add_post_meta($insert_event_post_id, 'contact_name', $event_contact_name, true)or
                    update_post_meta($insert_event_post_id, 'contact_name', $event_contact_name, true);
        }

        if (!empty($event_contact_title)) {
            add_post_meta($insert_event_post_id, 'contact_title', $event_contact_title, true)or
                    update_post_meta($insert_event_post_id, 'contact_title', $event_contact_title, true);
        }

        if (!empty($event_street_address)) {
            add_post_meta($insert_event_post_id, 'street_address', $event_street_address, true)or
                    update_post_meta($insert_event_post_id, 'street_address', $event_street_address, true);
        }

        if (!empty($event_city)) {
            add_post_meta($insert_event_post_id, 'city', $event_city, true)or
                    update_post_meta($insert_event_post_id, 'city', $event_city, true);
        }

        if (!empty($event_zipcode)) {
            add_post_meta($insert_event_post_id, 'zipcode', $event_zipcode, true)or
                    update_post_meta($insert_event_post_id, 'zipcode', $event_zipcode, true);
        }

        if (!empty($event_phone_number)) {
            add_post_meta($insert_event_post_id, 'phone_no', $event_phone_number, true)or
                    update_post_meta($insert_event_post_id, 'phone_no', $event_phone_number, true);
        }

        if (!empty($event_email_id)) {
            add_post_meta($insert_event_post_id, 'email_id', $event_email_id, true)or
                    update_post_meta($insert_event_post_id, 'email_id', $event_email_id, true);
        }

        if (!empty($event_url)) {
            add_post_meta($insert_event_post_id, 'url', $event_url, true)or
                    update_post_meta($insert_event_post_id, 'url', $event_url, true);
        }

        if (!empty($enter_event_tags)) {
            add_post_meta($insert_event_post_id, 'enter_tags', $enter_event_tags, true)or
                    update_post_meta($insert_event_post_id, 'enter_tags', $enter_event_tags, true);
        }

        if (!empty($start_event_date)) {
            add_post_meta($insert_event_post_id, 'start_date_time', $start_event_date, true)or
                    update_post_meta($insert_event_post_id, 'start_date_time', $start_event_date, true);
        }

        if (!empty($end_event_date)) {
            add_post_meta($insert_event_post_id, 'end_date_time', $end_event_date, true)or
                    update_post_meta($insert_event_post_id, 'end_date_time', $end_event_date, true);
        }

        if (!empty($event_address1)) {
            add_post_meta($insert_event_post_id, 'address_line_1', $event_address1, true)or
                    update_post_meta($insert_event_post_id, 'address_line_1', $event_address1, true);
        }

        if (!empty($event_address2)) {
            add_post_meta($insert_event_post_id, 'address_line_2', $event_address2, true)or
                    update_post_meta($insert_event_post_id, 'address_line_2', $event_address2, true);
        }

        if (!empty($event_address3)) {
            add_post_meta($insert_event_post_id, 'address_line_3', $event_address3, true)or
                    update_post_meta($insert_event_post_id, 'address_line_3', $event_address3, true);
        }

        if (!empty($typeofevent)) {
            add_post_meta($insert_event_post_id, 'type_of_event', $typeofevent, true)or
                    update_post_meta($insert_event_post_id, 'type_of_event', $typeofevent, true);
        }

        if (!empty($event_tax_id)) {
            add_post_meta($insert_event_post_id, 'tax_id', $event_tax_id, true)or
                    update_post_meta($insert_event_post_id, 'tax_id', $event_tax_id, true);
        }

        if (!empty($behavior_event)) {
            add_post_meta($insert_event_post_id, 'behavior_of_event', $behavior_event, true)or
                    update_post_meta($insert_event_post_id, 'behavior_of_event', $behavior_event, true);
        }

        if (!empty($no_volunteer)) {
            add_post_meta($insert_event_post_id, 'no_volunteers', $no_volunteer, true)or
                    update_post_meta($insert_event_post_id, 'no_volunteers', $no_volunteer, true);
        }

        if (!empty($fundraising_goal_event)) {
            add_post_meta($insert_event_post_id, 'fundraising_goal_event', $fundraising_goal_event, true)or
                    update_post_meta($insert_event_post_id, 'fundraising_goal_event', $fundraising_goal_event, true);
        }


        $event_success_msg = 'Event post added successfully';
    } else {
        $event_success_msg = 'Event post not added successfully';
    }
}
?>
<!-- cause-form -->
<div class="container-fluid cause-banner">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h2>Plan Your Cause</h2>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et.</p>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="causeFormWrap">
            <h2>Getting Started</h2>

            <div class="col-sm-12 col-md-8 col-md-offset-2 ">
                <div class="cause-form">
                    <div class="innerGap">
                        <!--tab nav-->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                    Cause</a></li>
                            <li role="presentation">
                                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Event</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->

                        <!-- =================== Add cause post start ===========================  -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <h3>Describe the Organization</h3>
                                <?php
                                if (!empty($cause_success_msg)) {
                                    echo '<h4>' . $cause_success_msg . '</h4>';
                                }
                                ?>
                                <form class="form-horizontal causeForm" name="causeForm" id="causeForm" method="post" enctype="multipart/form-data" action=""> 
                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Organization Name:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="cause_organization_name" id="cause_organization_name" placeholder="Enter the charity name">
                                        </div>
                                    </div>

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Contact Info:</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="cause_contact_name" id="cause_contact_name" placeholder="Contact Name">
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="cause_contact_title" id="cause_contact_title"  placeholder="Contact Title">
                                        </div>
                                    </div>

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Postal Address:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control"  name="cause_street_address" id="cause_street_address"  placeholder="Enter the Street address">
                                            <input type="text" class="form-control"  name="cause_city"  id="cause_city"   placeholder="Enter the name of the City">
                                            <div class="selectBox">
                                                <select class="form-control" name="cause_state" id="cause_state">
                                                    <option value="">Select state</option>
                                                    <?php
                                                    $taxonomies = get_terms('state_category', array(
                                                        'orderby' => 'count',
                                                        'hide_empty' => 0,
                                                    ));
                                                    if ($taxonomies) {
                                                        foreach ($taxonomies as $taxonomy) {
                                                            echo '<option value="' . $taxonomy->slug . '">' . $taxonomy->name . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div id="cause_state_error" class="error"></div>
                                            </div>
                                            <input type="text" class="form-control" name="cause_zipcode" id="cause_zipcode" placeholder="Enter Zipcode">
                                        </div>
                                    </div>

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Additional Info:</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="cause_phone_no"  id="cause_phone_no" placeholder="Enter Phone No.">
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="email" class="form-control" name="cause_email_id"  id="cause_email_id"  placeholder="Enter Email Id">
                                        </div>
                                        <div class="col-sm-4 col-md-push-4">
                                            <input type="text" class="form-control" name="cause_url" id="cause_url" placeholder="Enter URL">
                                        </div>
                                    </div>



                                    <h3>Describe the Cause</h3>    

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Cause Name:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="cause_name"  id="cause_name" placeholder="Enter the name of the cause">
                                        </div>
                                    </div>

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Category of the Cause:</label>
                                        <div class="col-sm-8">
                                            <div class="selectBox">
                                                <select class="form-control" name="category_of_the_cause"  id="category_of_the_cause">
                                                    <option value="">Select Cause Category</option>
                                                    <?php
                                                    $taxonomies = get_terms('cause_category', array(
                                                        'orderby' => 'count',
                                                        'hide_empty' => 0,
                                                    ));

                                                    if ($taxonomies) {
                                                        foreach ($taxonomies as $taxonomy) {

                                                            echo '<option value="' . $taxonomy->slug . '">' . $taxonomy->name . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div id='cause_category_error' class="error"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Enter Tags:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="enter_tags" id="enter_tags"  placeholder="e.g., animal, big cats, tiger etc.">
                                        </div>
                                    </div>
                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">End:</label>
                                        <div class="col-sm-3">
                                            <input type="test" class="form-control" placeholder="Date" name="end_cause_date"  id="end_cause_date">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" placeholder="Time" name="end_cause_time" id="end_cause_time">
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="selectBox">
                                                <select class="form-control"  name="end_cause_am_pm" id="end_cause_am_pm">                   
                                                    <option value="">AM/PM</option>
                                                    <option value="AM">AM</option>
                                                    <option value="PM">PM</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Description:</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" name="additional_info" id="additional_info" placeholder="Enter description of the cause"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Type of Cause:</label>
                                        <div class="col-sm-2">
                                            <input type="radio" name="type_of_cause"  id="type_of_cause"  class="radioBox" value="Private">
                                            <label class="radioLabel"> Private </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="radio" name="type_of_cause"  id="type_of_cause"   value="Non-profit"><label class="radioLabel"> Non-profit</label> 
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" name="tax_id"  id="tax_id"  placeholder="Enter Tax ID" style="display:none">
                                        </div>     
                                        <div id="type_of_cause_error" class="error"></div>              
                                    </div>


                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Fundraising Goal :</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name="fundraising_goal"  id="fundraising_goal"  placeholder="Enter amount">
                                        </div>
                                        <div class="col-sm-1">
                                            <span class="smallText">USD</span>
                                        </div>
                                    </div>

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Upload Image:</label>
                                        <div class="col-sm-8">
                                         <!-- <div class="flile-control"><input type="file" name="file_cause" id="file_cause" class="form-control"></div> -->
                                            <div class="flile-control"><input type="file" name="file_cause" id="file_cause" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple />
                                                <label for="file_cause"><img src="<?php echo site_url(); ?>/wp-content/themes/saveforplanet/images/imgIcon.png" alt=""> <span>Browse Images From Computer</span></label></div>
                                        </div>
                                    </div>
                                    <div class="form-group">       
                                        <label class="col-sm-4 control-label"></label>
                                        <div class="col-sm-8">
                                            <div id="dvPreview"></div>
                                        </div>
                                    </div>
                                    <div id="file1_error"  class="error"></div>
                                    <footer class="formFoot">
                                        <input type="reset" value="Go Back">
                                        <input type="submit"  id="join_cause" name="join_cause" value="Save and Publish">
                                    </footer>

                                </form>

                            </div>







                            <div role="tabpanel" class="tab-pane" id="profile">
                                <h3>Describe the Organization</h3>
                                <?php
                                if (!empty($event_success_msg)) {
                                    echo '<h4>' . $event_success_msg . '</h4>';
                                }
                                ?>

                                <form class="form-horizontal causeForm" name="eventForm" id="eventForm" method="post" enctype="multipart/form-data" action="">  
                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Organization Name:</label>
                                        <div class="col-sm-8">                      
                                            <input type="text" class="form-control" name="event_organization_name" id="event_organization_name" placeholder="Enter the charity name">
                                        </div>
                                    </div>

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Contact Info:</label>
                                        <div class="col-sm-4">                      
                                            <input type="text" class="form-control" name="event_contact_name" id="event_contact_name" placeholder="Contact Name">
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control"  name="event_contact_title" id="event_contact_title"  placeholder="Contact Title">
                                        </div>
                                    </div>

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Postal Address:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="event_street_address" id="event_street_address" placeholder="Enter the Street address">
                                            <input type="text" class="form-control" name="event_city"  id="event_city"   placeholder="Enter the name of the City">
                                            <div class="selectBox">
                                                <select class="form-control" name="event_state" id="event_state">
                                                    <option value="">Select state</option>
                                                    <?php
                                                    $taxonomies = get_terms('state_category', array(
                                                        'orderby' => 'count',
                                                        'hide_empty' => 0,
                                                    ));
                                                    if ($taxonomies) {
                                                        foreach ($taxonomies as $taxonomy) {
                                                            echo '<option value="' . $taxonomy->slug . '">' . $taxonomy->name . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div id="event_state_error" class="error"></div>
                                            <input type="text" class="form-control"  name="event_zipcode" id="event_zipcode"  placeholder="Enter Zipcode">
                                        </div>
                                    </div>

                                    <div class="form-group">   <!-- Event addtional info start  -->     
                                        <label  class="col-sm-4 control-label">Additional Info:</label>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="event_phone_number" id="event_phone_number" placeholder="Enter Phone No.">
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="event_email_id" id="event_email_id" placeholder="Enter Email Id">
                                        </div>
                                        <div class="col-sm-4 col-md-push-4">
                                            <input type="text" class="form-control" name="event_url" id="event_url"  placeholder="Enter URL">
                                        </div>
                                    </div> <!-- Event addtional info end  -->   



                                    <h3>Describe the Event</h3>    

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Event Name:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control"  name="event_name" id="event_name" placeholder="Enter the name of the cause">
                                        </div>
                                    </div>
                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Category of the Cause:</label>
                                        <div class="col-sm-8">
                                            <div class="selectBox">
                                                <select class="form-control" name="category_of_the_event" id="category_of_the_event">
                                                    <option value="">Select Cause Category</option>
                                                    <?php
                                                    $taxonomies = get_terms('cause_category', array(
                                                        'orderby' => 'count',
                                                        'hide_empty' => 0,
                                                    ));
                                                    if ($taxonomies) {
                                                        foreach ($taxonomies as $taxonomy) {
                                                            echo '<option value="' . $taxonomy->slug . '">' . $taxonomy->name . '</option>';
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div id='event_category_error' class="error"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Enter Tags:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="enter_event_tags" id="enter_event_tags"  placeholder="e.g., animal, big cats, tiger etc.">
                                        </div>
                                    </div>

                                    <div class="form-group">    <!-- Event Start Info start -->   
                                        <label  class="col-sm-4 control-label">Start:</label>
                                        <div class="col-sm-3">
                                            <input type="date" class="form-control" name="start_event_date" id="start_event_date" placeholder="Date">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="time" class="form-control" name="start_event_time" id="start_event_time" placeholder="Time">
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="selectBox">
                                                <select class="form-control" name="start_event_am_pm" id="start_event_am_pm">
                                                    <option value="">AM/PM</option>
                                                    <option value="AM">AM</option>
                                                    <option value="PM">PM</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>        <!-- Event Start Info end -->  

                                    <div class="form-group">    <!-- Event End Info start -->     
                                        <label  class="col-sm-4 control-label">End:</label>
                                        <div class="col-sm-3">
                                            <input type="date" class="form-control" placeholder="Date" name="end_event_date"  id="end_event_date">
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="time" class="form-control" placeholder="Time" name="end_event_time" id="end_event_time">
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="selectBox">
                                                <select class="form-control"  name="end_event_am_pm" id="end_event_am_pm">                   
                                                    <option value="">AM/PM</option>
                                                    <option value="AM">AM</option>
                                                    <option value="PM">PM</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Description:</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" name="additional_info_event" id="additional_info_event" placeholder="Enter description of the event"></textarea>
                                        </div>
                                    </div>
                                    <!-- Event End Info end -->  

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Location:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" name="event_address1" id="event_address1"  placeholder="Enter Address Line 1">
                                            <input type="text" class="form-control" name="event_address2" id="event_address2"  placeholder="Enter Address Line 2">
                                            <input type="text" class="form-control" name="event_address3" id="event_address3" placeholder="Enter Address Line 3">
                                        </div>
                                    </div>


                                    <div class="form-group">    <!-- Type Of Event Start panel -->    
                                        <label  class="col-sm-4 control-label">Type of Event:</label>
                                        <div class="col-sm-3">
                                            <input type="radio" name="typeofevent" id="typeofevent" class="radioBox" value="Individual">
                                            <label class="radioLabel"> Individual </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="radio"  name="typeofevent" id="typeofevent" name="radio"  class="radioBox"   value="Charity">
                                            <label class="radioLabel"> Charity</label> 
                                        </div>
                                        <div class="col-sm-3">
                                            <input type="text" class="form-control" name="event_tax_id" id="event_tax_id" placeholder="Enter Tax ID" style="display:none">
                                        </div>
                                        <div id="typeofevent_error"  class="error"></div>
                                    </div> <!-- Type Of Event end panel -->   

                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Behavior of Event:</label>
                                        <div class="col-sm-3">
                                            <div class="checkboxHolder checkbox">
                                                <input type="checkbox" name="behavior_event[]"  id="behavior_event_volunteer" value="Volunteer" class="behavior_event_class">
                                                <label class="checkLabel">Volunteer</label>
                                            </div>  
                                        </div>

                                        <div class="col-sm-5">
                                            <input type="text"  name="no_volunteer" id="no_volunteer"  class="form-control" placeholder="No. of volunteers" style="display:none">
                                        </div>                    
                                    </div>
                                    <div class="form-group">       
                                        <label  class="col-sm-4" > </label>
                                        <div class="col-sm-3">
                                            <div class="checkboxHolder checkbox">
                                                <input type="checkbox"  name="behavior_event[]" id="behavior_event_fund" value="Fundraiser" class="behavior_event_class">
                                                <label class="checkLabel">Fundraiser</label>
                                            </div>  
                                            <div id="behavior_event_error" class="error"></div>
                                        </div>

                                        <div class="col-sm-5">
                                            <input type="text"  name="fundraising_goal_event" id="fundraising_goal_event" class="form-control" placeholder="Enter Fundraiser Goal(USD)" style="display:none">
                                        </div>                    
                                    </div>
                                    <div id="behavior_cause_error" class="error"></div>
                                    <div class="form-group">       
                                        <label  class="col-sm-4 control-label">Upload Image:</label>
                                        <div class="col-sm-8">
                                          <!--<div class="flile-control"><input type="file" class="form-control"></div>-->
                                            <div class="flile-control">
                                                <input type="file" name="file_event" id="file_event" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple />
                                                <label for="file_event">
                                                    <img src="<?php echo site_url(); ?>/wp-content/themes/saveforplanet/images/imgIcon.png" alt="">
                                                    <span>Browse Images From Computer</span></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">       
                                        <label class="col-sm-4 control-label"></label>
                                        <div class="col-sm-8">
                                            <div id="dvPreview1"></div>
                                        </div>
                                    </div>
                                    <div id="file_event_error"  class="error"></div>
                                    <footer class="formFoot">
                                        <input type="reset" value="Go Back">
                                        <input type="submit" value="Publish event" id="event_submit" name="event_submit">
                                    </footer>

                                </form>

                            </div>
                        </div> 
                    </div> 
                </div>

            </div>
        </div>
    </div>
</div>
<!-- cause-form end --> 

<!-- Logo Add End --> 
<?php get_footer(); ?>