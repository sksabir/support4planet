<?php
/**
 * Template Name:Support template
 */
get_header();
?>

<div class="container donor-comment-section">
    <div class="row">
        <!-- Graph TK --> 

        <div class="col-sm-12 col-md-8 col-md-offset-2 graph-tk">
            <h2><?php the_title(); ?></h2>
        </div>

        <div class="col-sm-12 col-md-12 inner-contentarea">
            <?php $sections = get_field('sections', get_the_ID()); ?>
            <?php
            if (count($sections)) {
                foreach ($sections as $key => $section) {
                    ?>

                    <p>&nbsp;</p>
                    <?php echo '<h3>' . $section['section_title'] . '</h3>'; ?>
                    <?php echo apply_filters('the_content', $section['section_content']); ?>

                <?php
                }
            }
            ?>

        </div>


    </div>

</div>
<!-- Logo Add End --> 
<?php get_footer(); ?>