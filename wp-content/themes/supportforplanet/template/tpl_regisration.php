<?php
/**
 * Template Name:Signup template
 */
if (is_user_logged_in()) {
    $user_ID = get_current_user_id();
    $user = get_userdata($user_ID);
    ?>
    <script type="text/javascript">window.location.href = "<?php echo site_url(); ?>/edit-profile/";</script>
    <?php
}
$msg = '';
if ($_POST['email_id'] != '') {
    if (email_exists($_POST['email_id']) == false) {
        $user_mail = explode('@', $_POST['email_id']);
        $userdata = array(
            'user_login' => $_POST['email_id'],
            'user_pass' => $_POST['pass'], // When creating an user, `user_pass` is expected.
            'user_email' => $_POST['email_id'],
            'user_url' => $_POST['website'],
            'role' => $_POST['role']
        );
        $user_id = wp_insert_user($userdata);
        update_field('field_56de7e95563cd', $_POST['org_name'], 'user_' . $user_id);
        update_field('field_56de7e139d862', $_POST['phone'], 'user_' . $user_id);
        update_field('field_56f8bd463f2f0', date('Ymd h:i:s'), 'user_' . $user_id);

        update_user_meta($user_id, 'first_name', $_POST['fName']);
        update_user_meta($user_id, 'last_name', $_POST['lName']);
        update_user_meta($user_id, 'raw_password', $_POST['pass']);

        $user = get_user_by('id', $user_id);
        if ($user) {
            wp_set_current_user($user_id, $user->user_login);
            wp_set_auth_cookie($user_id);
            do_action('wp_login', $user->user_login);
        }

        $msg = '<p style="color:#0c0;">You are successfully registered</p><script type="text/javascript">window.location.href = "' . site_url() . '/edit-profile/";</script>';
    } else {
        $msg = '<p style="color:#c00;">User already exists</p>';
    }
}

get_header();
wp_reset_query();
?>
<style type="text/css">
    .error-msg {
        border: 1px solid red !important;
    }
    .error {
        color: red;
    }
</style>
<!-- Login start -->
<section class="loginSec">
    <div class="container">

        <h2><?php the_title(); ?></h2>
        <?php echo $msg; ?>
        <p><?php the_content(); ?></p>

        <div class="loginformWrap">
            <div class="loginForm">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#individual" aria-controls="home" role="tab" data-toggle="tab">
                            <span class="indi"></span> I am an Individual</a></li>
                    <li role="presentation">
                        <a href="#organization" aria-controls="profile" role="tab" data-toggle="tab"><span class="org"></span> We are an Organization</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="individual">
                        <h3>Describe Yourself</h3>
                        <form id="registration-form-indi" action="" varify-email="<?php echo site_url(); ?>/ajax/varify-email.php" class="form-horizontal registration-form" method="post">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Your Name:</label>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="fName" placeholder="First Name">
                                </div>
                                <div class="col-sm-4">
                                    <input class="form-control" type="text" name="lName" placeholder="Last Name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Your Email:</label>
                                <div class="col-sm-8">
                                    <input class="form-control email_id" form-id="registration-form-indi" id="email_id_indi" name="email_id" type="text" placeholder="Enter email address">
                                </div>
                            </div>

                            <div id="var_code_indi" style="display: none;" class="form-group var_code"></div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Password:</label>
                                <div class="col-sm-8">
                                    <input class="form-control pass" id="indi-pass" name="pass" type="password" placeholder="*******">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Re-Enter Password:</label>
                                <div class="col-sm-8">
                                    <input class="form-control pass-conf" id="indi-pass-conf" type="password" placeholder="*******">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Phone Number:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" name="phone" placeholder="Enter Number">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    <p class="smText">Already have an account? <a href="<?php echo site_url(); ?>/log-in/">Login</a></p>
                                </div>
                                <div class="col-sm-4">
                                    <input type="hidden" name="role" value="individual" />
                                    <input type="submit" class="yellowBtn" value="Sign Up"> 
                                </div>
                            </div>

                        </form>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="organization">
                        <h3>Describe The Organization</h3>
                        <form id="registration-form-org" action="" varify-email="<?php echo site_url(); ?>/ajax/varify-email.php" method="post" class="form-horizontal registration-form">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Organization Name:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="org_name" type="text" placeholder="Enter the charity name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Website:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="website" type="text" placeholder="Enter the website link">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Contact Info:</label>
                                <div class="col-sm-4">
                                    <input class="form-control" name="fName" type="text" placeholder="First Name">
                                </div>
                                <div class="col-sm-4">
                                    <input class="form-control" name="lName" type="text" placeholder="Last Name">
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-sm-4 control-label">Your Email:</label>
                                <div class="col-sm-8">
                                    <input class="form-control email_id" form-id="registration-form-org" id="email_id_org" name="email_id"  type="text" placeholder="Enter email address">
                                </div>
                            </div>

                            <div id="var_code_org" style="display: none;" class="form-group var_code"></div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Password:</label>
                                <div class="col-sm-8">
                                    <input class="form-control pass" id="org-pass" name="pass" type="password" placeholder="*******">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Re-Enter Password:</label>
                                <div class="col-sm-8">
                                    <input class="form-control pass-conf" id="org-pass-conf" type="password" placeholder="*******">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label">Phone Number:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="phone" type="text" placeholder="Enter Number">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    <p class="smText">Already have an account? <a href="<?php echo site_url(); ?>/log-in/">Login</a></p>
                                </div>
                                <div class="col-sm-4">
                                    <input type="hidden" name="role" value="organization" />
                                    <input type="submit" class="yellowBtn" value="Sign Up"> 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- login end --> 

<?php
wp_enqueue_script('register_user');
get_footer();
?>