<?php
/**
 * Template Name:Couse template
 */
get_header();
?>
<?php $the_query = new WP_Query('page_id=9'); ?>
<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
    <?php the_content(); ?>
<?php endwhile; ?>
<div class="container featured">
    <div class="row">
        <div class="col-sm-7 col-md-7">
            <div class="col-sm-7 no-padding">
                <ul id="filters" class="clearfix">
                    <li><span class="filter active" data-filter="app card icon logo web">All Causes </span></li>
                    <li><span class="filter" data-filter="app">What’s Trending</span></li>
                    <li><span class="filter" data-filter="card">Latest</span></li>
                </ul>
            </div>
            <div class="col-sm-5 no-padding">
                <div class="select-one">
                    <p>Categories:</p>
                    <label>
                        <select>
                            <option selected> Animal</option>
                            <option>text 1</option>
                            <option>text 1</option>
                        </select>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-sm-5 col-md-5">
            <div class="map-direction">
                <p>Find events near you:</p>
                <form>
                    <div class="input-group">
                        <input type="text" class="form-control input-search" placeholder="Enter your zipcode">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-primary">Go</button>
                        </span> </div>
                </form>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 view-cell">
            <div id="portfoliolist">
                <div style=" display: inline-block; opacity: 1;" class="portfolio logo mix_all" data-cat="logo">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-1.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio app mix_all" data-cat="app">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-2.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio web mix_all" data-cat="web">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-3.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio card mix_all" data-cat="card">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-4.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio app mix_all" data-cat="app">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-5.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio card mix_all" data-cat="card">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-6.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio card mix_all" data-cat="card">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-1.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio logo mix_all" data-cat="logo">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-2.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio app mix_all" data-cat="app">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-1.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio card mix_all" data-cat="card">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-2.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio logo mix_all" data-cat="logo">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-4.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio logo mix_all" data-cat="logo">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-6.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio icon mix_all" data-cat="icon">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-7.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio web mix_all" data-cat="web">
                    <div class="featured-causes-box"> <img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-2.jpg">
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio icon mix_all" data-cat="icon">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-5.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio web mix_all" data-cat="web">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-3.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio icon mix_all" data-cat="icon">
                    <div class="featured-causes-box"><a href=""> <img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-6.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio icon mix_all" data-cat="icon">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-4.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio web mix_all" data-cat="web">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-7.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2>MayDOG Park Improvement Fund</h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio logo mix_all" data-cat="logo">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-6.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio logo mix_all" data-cat="logo">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-7.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio icon mix_all" data-cat="icon">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-8.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio card mix_all" data-cat="card">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-7.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
                <div style=" display: inline-block; opacity: 1;" class="portfolio logo mix_all" data-cat="logo">
                    <div class="featured-causes-box"><a href=""><img alt="" src="http://localhost/saveforplanet/wp-content/themes/saveforplanet/images/post-9.jpg"></a>
                        <div class="shear"><a href="">like</a></div>
                        <div class="docoment-section">
                            <h2><a href="">MayDOG Park Improvement Fund</a></h2>
                            <p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>
                            <span class="graph"> <span class="pull-right">15%</span>
                                <div class="progress progress-warning active">
                                    <div style="width: 15%;" class="bar"></div>
                                </div>
                                <strong>12 Days Left</strong> </span> </div>
                        <div class="grey-support">
                            <ul>
                                <li><span class="rupee"></span>
                                    <p>Funded 80%</p>
                                </li>
                                <li><span class="doller"></span>
                                    <p>Pledged $2453</p>
                                </li>
                            </ul>
                            <div class="supprot"><a href="">Support</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="" class="view-causes">View More Causes</a> </div>
    </div>
</div>
<!-- Featured end --> 
<?php get_footer(); ?>