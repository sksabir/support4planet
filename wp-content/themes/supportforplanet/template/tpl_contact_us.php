<?php
/**
 * Template Name:Contact Us template
 */
get_header();
?>
<div class="container-fluid contact-banner">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h2><?php echo get_field("title"); ?></h2>
                <?php echo get_field("description"); ?>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1 contact-form">
            <?php echo get_field("contact_us_block"); ?>
        </div>
    </div>
</div>
<div class="container-fluid get-touch">
    <div class="container touch-inner">
        <div class="row">
            <div class="col-sm-12 col-md-10 col-md-offset-1">
                <h2><?php echo get_field("get_in_touch_title"); ?></h2>
                <?php echo get_field("description_get_in_touch"); ?>
                <div class="col-sm-3 col-md-3 ">
                    <div class="call_hk">

                        <span class="call-point"></span>
                        <h2><?php echo get_field("call_us_title"); ?></h2>
                        <?php echo get_field("call_us_text"); ?>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="call_hk">

                        <span class="map-point"></span>
                        <h2><?php echo get_field("visit_us_title"); ?></h2>
                        <?php echo get_field("visit_us_text"); ?>

                    </div>
                </div>
                <div class="col-sm-3 col-md-3 ">
                    <div class="call_hk">

                        <span class="email-point"></span>
                        <h2><?php echo get_field("mail_us_title"); ?></h2>
                        <?php echo get_field("mail_us_text"); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>