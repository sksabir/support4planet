<?php
/**
 * Template Name:Forget Password template
 */
if (is_user_logged_in()) {
    ?>
    <script type="text/javascript">window.location.href = "<?php echo site_url(); ?>/edit-profile/";</script>
    <?php
}
$msg = '';
if ($_POST['email_id'] != '') {
    if (!email_exists($_POST['email_id'])) {
        $msg = '<p style="color:#c00;">Email does not exist.</p>';
    } else {
        $user = get_user_by('email', $_POST['email_id']);
        $pass = get_user_meta($user->ID, 'raw_password', true);
        $to = $_POST['email_id'];
        $subject = "Forget Email";

        $message = "
		<html>
		<head>
		<title>Forget Email</title>
		</head>
		<body>
		<p>Your password is [<b>$pass</b>]</p>
		<p>Thank You.</p>
		</body>
		</html>
		";

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: Support for Planet<admin@supportforplanet.com>' . "\r\n";

        $report = mail($to, $subject, $message, $headers);
        if ($report) {
            $msg = '<p style="color:#0c0;">Password sent to your email.</p>';
        } else {
            $msg = '<p style="color:#c00;">Error</p>';
        }
    }
}
get_header();
wp_reset_query();
?>
<style type="text/css">
    .error-msg {
        border: 1px solid red !important;
    }
    .error {
        color: red;
    }
</style>
<section class="loginSec">
    <div class="container">

        <h2><?php the_title(); ?></h2>
        <?php echo $msg; ?>
        <p><?php the_content(); ?></p>

        <div class="loginformWrap">
            <form class="loginForm form-horizontal" action="" method="post">
                <div class="form-group">
                    <label class="col-sm-4 control-label">Email:</label>
                    <div class="col-sm-8">
                        <input type="text" name="email_id" placeholder="Enter the email address" class="form-control email_id">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-4"></div>
                    <div class="col-sm-4">
                        <p class="smText">Doesn’t have an account? <a href="<?php echo site_url(); ?>/create-account/">Sign Up</a></p>
                    </div>
                    <div class="col-sm-4">
                        <input type="submit" value="Submit" class="btn btn-primary"> 
                    </div>
                </div>

            </form>

        </div>

    </div>
</section>
<!-- login end --> 
<?php
wp_enqueue_script('user_login');
get_footer();
?>