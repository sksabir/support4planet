<?php
/**
 * Template Name:About us template
 */
get_header();
?>
<!-- cause-banner -->
<div class="container-fluid secand-about-banner">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <h2><?php echo get_field("title"); ?></h2>
                <?php echo get_field("description"); ?>
            </div>
        </div>
    </div>
</div>
<!-- cause-banner end --> 
<!-- header end --> 
<!-- Featured -->
<div class="container featured">
    <div class="row">
        <!-- Graph TK --> 
        <div class="col-sm-12 col-md-10 col-md-offset-1 graph-tk">
            <h2><?php echo get_field("we_helped_raise_title"); ?></h2>
            <?php echo get_field("we_helped_raise_description"); ?>
            <div class="graph-box">
                <img src="<?php echo get_field("we_helped_raise_image"); ?>"  alt="We Helped Raise"/> 
                <span class="doller-price"><p><?php echo get_field("we_helped_raise_price"); ?></p></span>
            </div>
            <a class="join-cause" href="<?php echo get_field("donate_now_link"); ?>"><?php echo get_field("donate_now_level"); ?></a>
        </div>
        <!-- Graph TK End -->
    </div>
</div>
<!-- Post Bule --> 
<div class="container-fluid post-bule">
    <div class="container">
        <div class="col-sm-4 col-md-4 post-contaner">
            <img src="<?php echo get_field("image_mission1"); ?>"  alt="Our Mission"/> 
            <h2 class="mission-two"><?php echo get_field("mission_title1"); ?></h2>
            <?php echo get_field("mission_description1"); ?>
        </div>
        <div class="col-sm-4 col-md-4 post-contaner">
            <img src="<?php echo get_field("image_mission2"); ?>"  alt="Our Vision"/> 
            <h2 class="vision-two"><?php echo get_field("mission_title2"); ?></h2>
            <?php echo get_field("mission_description2"); ?>
        </div>
        <div class="col-sm-4 col-md-4 post-contaner">
            <img src="<?php echo get_field("image_mission3"); ?>"  alt="Our Promise"/> 
            <h2 class="promise-two"><?php echo get_field("mission_title3"); ?></h2>
            <?php echo get_field("mission_description3"); ?>
        </div>
    </div>
</div>
<!-- Post Bule End --> 
<!-- Featured -->
<div class="container-fluid featured">
    <div class="container">
        <div class="row">
            <!-- Our Values -->
            <div class="col-sm-12 col-md-10 col-md-offset-1 community-body">
                <h2><?php echo get_field("title_our_values"); ?></h2>
                <ul>
                    <li>
                        <span class="valu-community yellow-one"></span>
                        <div class="community-inner">
                            <h2><?php echo get_field("title_community1"); ?></h2>
                            <?php echo get_field("description_community1"); ?>
                        </div></li>
                    <li>
                        <span class="valu-community yellow-two"></span>
                        <div class="community-inner">
                            <h2><?php echo get_field("title_alignment"); ?></h2>
                            <?php echo get_field("description_alignment"); ?>
                        </div></li>
                    <li>
                        <span class="valu-community yellow-three"></span>
                        <div class="community-inner">
                            <h2><?php echo get_field("title_caring"); ?></h2>
                            <?php echo get_field("description_caring"); ?>

                        </div></li>
                    <li>
                        <span class="valu-community yellow-four"></span>
                        <div class="community-inner">
                            <h2><?php echo get_field("title_trust"); ?></h2>
                            <?php echo get_field("description_trust"); ?>

                        </div></li>
                    <li>
                        <span class="valu-community yellow-five"></span>
                        <div class="community-inner">
                            <h2><?php echo get_field("title_hope"); ?></h2>
                            <?php echo get_field("description_hope"); ?>
                        </div></li>
                </ul>
            </div>
            <!-- Our Values End -->
        </div>
    </div>
</div>
<!-- How you can help -->
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1 help-section top-m">
            <h2><?php echo get_field("title_how_you_can_help"); ?></h2>
            <div class="col-sm-4 together"> <span class="helpothers"></span>
                <p> <?php echo get_field("sub_title_how_you_can_help1"); ?></p>
            </div>
            <div class="col-sm-4 together2"> <span class="cause"></span>
                <p> <?php echo get_field("sub_title_how_you_can_help2"); ?></p>
            </div>
            <div class="col-sm-4 together"> <span class="support"></span>
                <p> <?php echo get_field("sub_title_how_you_can_help3"); ?></p>
            </div>
        </div>
    </div>
</div>
<!-- How you can help End --> 
<!-- Serve Section --> 
<div class=" container-fluid serve-section">
    <div class="container">
        <div class="row">
            <h2><?php echo get_field("who_we_serve_title"); ?></h2>
            <div class="col-sm-4 col-md-4 donator">
                <span class="icon-vander-one"></span>
                <h3><?php echo get_field("title_donors"); ?></h3>
                <?php echo get_field("description_donors"); ?>
            </div>
            <div class="col-sm-4 col-md-4 donator">
                <span class="icon-vander-two"></span>
                <h3><?php echo get_field("title_volunteer"); ?></h3>
                <?php echo get_field("description_volunteer"); ?>
            </div>
            <div class="col-sm-4 col-md-4 donator">
                <span class="icon-vander-three"></span>
                <h3><?php echo get_field("title_charities"); ?></h3>
                <?php echo get_field("description_charities"); ?>
            </div>
        </div>
    </div>
</div>
<!-- Serve Section end --> 
<!-- Featured end --> 
<?php get_footer(); ?>