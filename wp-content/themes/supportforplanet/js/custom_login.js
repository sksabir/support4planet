jQuery(document).ready(function () {
    jQuery('.loginForm .form-control').keyup(function () {
        if (jQuery(this).val() == '') {
            jQuery(this).addClass('error-msg');
        } else {
            jQuery(this).removeClass('error-msg');
        }
    });
    jQuery(".loginForm").on('submit', function (e) {
        $form = jQuery(this);
        error = false;
        e.preventDefault();
        $form.find('.form-control').each(function () {
            if (jQuery(this).val() == '') {
                jQuery(this).addClass('error-msg');
                error = true;
            }
        });
        if (!error) {
            if (!validateEmail($form.find('.email_id').val())) {
                $form.find('.email_id').addClass("error-msg");
                $form.find('.email_id').focus();
                error = true;
            }
        }
        if (!error) {
            if ($form.find('.pass').val() != $form.find('.pass-conf').val())
            {
                $form.find('.pass-conf').val('');
                $form.find('.pass-conf').addClass('error-msg');
                $form.find('.pass-conf').focus();
                error = true;
            }
        }
        if (!error) {
            jQuery(this).unbind('submit').submit();
        }
    });
});