// JavaScript Document
$(document).ready(function () {
    $('.c_logo').owlCarousel({
        loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            1024: {
                items: 3,
                nav: true
            },
            768: {
                items: 2,
                nav: true
            },
            320: {
                items: 1,
                nav: true,
            }
        }
    })

});
$(document).ready(function () {
    $('.clientlogo').owlCarousel({
        autoplay: true,
        autoplayTimeout: 500,
        loop: true,
        margin: 10,
        responsiveClass: true,
        responsive: {
            1024: {
                items: 9,
                nav: true
            },
            768: {
                items: 4,
                nav: true
            },
            320: {
                items: 2,
                nav: true,
            }
        }
    })

});
$(function () {

    var filterList = {
        init: function () {

            // MixItUp plugin
            // http://mixitup.io
            $('#portfoliolist').mixitup({
                targetSelector: '.portfolio',
                filterSelector: '.filter',
                effects: ['fade'],
                easing: 'snap',
                // call the hover effect
                onMixEnd: filterList.hoverEffect()
            });

        },
        hoverEffect: function () {

            // Simple parallax effect
            $('#portfoliolist .portfolio').hover(
                    function () {
                        $(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
                        $(this).find('img').stop().animate({top: -30}, 500, 'easeOutQuad');
                    },
                    function () {
                        $(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
                        $(this).find('img').stop().animate({top: 0}, 300, 'easeOutQuad');
                    }
            );

        }

    };

    // Run the show!
    filterList.init();

    //Horizontal Tab
    $('#parentHorizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion
        width: 'auto', //auto or any width like 600px
        fit: true, // 100% fit in a container
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function (event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });

    function toggleChevron(e) {
        $(e.target)
                .prev('.panel-heading')
                .find("i.indicator")
                .toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    }
    $('#accordion').on('hidden.bs.collapse', toggleChevron);
    $('#accordion').on('shown.bs.collapse', toggleChevron);

    $("#datepicker").datepicker({
        autoclose: true,
        todayHighlight: true
    }).datepicker(new Date());
    $("#datepicker").find("input").val("");

    $("#datepicker2").datepicker({
        autoclose: true,
        todayHighlight: true
    }).datepicker(new Date());
    $("#datepicker2").find("input").val("");

    $("#datepicker3").datepicker({
        autoclose: true,
        todayHighlight: true
    }).datepicker(new Date());
    $("#datepicker3").find("input").val("");

    $('input[type=checkbox]').on('change', function () {
        ($('#checkbox-2').is(':checked')) && $('#checkedShowSelect').fadeIn() || $('#checkedShowSelect').fadeOut();
    });

    $('input[type=checkbox]').on('change', function () {
        ($('#checkbox-4').is(':checked')) && $('#checkedShowSelect-one').fadeIn() || $('#checkedShowSelect-one').fadeOut();
    });
    $('input[type=checkbox]').on('change', function () {
        ($('#checkbox-6').is(':checked')) && $('#checkedShowSelect-three').fadeIn() || $('#checkedShowSelect-three').fadeOut();
    });


    $('input[type=checkbox]').on('change', function () {
        ($('#checkbox-8').is(':checked')) && $('#checkedShowSelect-four').fadeIn() || $('#checkedShowSelect-four').fadeOut();
    });

});				