jQuery(document).ready(function () {
    jQuery(".all_cause_tab").click(function () {
        jQuery(".whats-pagi").hide();
        jQuery(".latest-pagi").hide();
    });
    jQuery(".whats_tending_tab").click(function () {
        jQuery(".all-pagi").hide();
        jQuery(".latest-pagi").hide();
        jQuery(".whats-pagi").show();
    });
    jQuery(".latest_tab").click(function () {
        jQuery(".all-pagi").hide();
        jQuery(".whats-pagi").hide();
        jQuery(".latest-pagi").show();
    });
    jQuery("#all-pagi").click(function (e) {
        e.preventDefault();
        var page = jQuery(this).attr("page");
        var type = jQuery(this).attr("type");
        var nextpage = parseInt(page) + 1;
        jQuery.ajax({
            url: 'http://c04.481.myftpupload.com/wp-admin/admin-ajax.php',
            type: 'post',
            data: {action: 'all_pagination', page: page, type: type},
            success: function (response) {
                alert(response);
                if (response == '')
                {
                    jQuery(".all-view-causes").hide();
                } else {
                    jQuery("#portfoliolist").append(response);
                    jQuery("#all-pagi").removeAttr("page");
                    jQuery("#all-pagi").attr("page", nextpage);
                }


            }
        });
    });
})