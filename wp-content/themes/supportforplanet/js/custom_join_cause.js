//form cause
function validateEmail(sEmail) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
        return true;
    } else {
        return false;
    }
}
jQuery(document).ready(function () {
    //*for cause form*/
    jQuery("#file_cause").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = jQuery("#dvPreview");
            dvPreview.html("");
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            jQuery(jQuery(this)[0].files).each(function () {
                var file = jQuery(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = jQuery("<img />");
                        img.attr("style", "height:100px;width: 150px");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    alert(file[0].name + " is not a valid image file.");
                    dvPreview.html("");
                    return false;
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
    jQuery('input[name=type_of_cause]').click(function () {
        jQuery("#type_of_cause_error").html('');
        if (jQuery('input[name=type_of_cause]:checked').val() == 'Non-profit')
        {
            jQuery("#tax_id").show();
        } else {
            jQuery("#tax_id").hide();
        }
    })
    jQuery(".errorSpan").text("");
    jQuery('#join_cause').click(function (event) {
        //event.preventDefault();
        var flag = 0;
        jQuery("#cause_organization_name").removeClass("error-msg");
        jQuery("#cause_contact_name").removeClass("error-msg");
        jQuery("#cause_contact_title").removeClass("error-msg");
        jQuery("#cause_street_address").removeClass("error-msg");
        jQuery("#cause_city").removeClass("error-msg");
        jQuery("#cause_state").removeClass("error-msg");
        jQuery("#cause_zipcode").removeClass("error-msg");

        jQuery("#cause_phone_no").removeClass("error-msg");
        jQuery("#cause_email_id").removeClass("error-msg");
        jQuery("#cause_url").removeClass("error-msg");
        jQuery("#cause_name").removeClass("error-msg");




        jQuery("#enter_tags").removeClass("error-msg");
        jQuery("#additional_info").removeClass("error-msg");

        jQuery("#end_cause_date").removeClass("error-msg");
        jQuery("#end_cause_time").removeClass("error-msg");
        jQuery("#end_cause_am_pm").removeClass("error-msg");

        jQuery("#tax_id").removeClass("error-msg");
        jQuery("#fundraising_goal").removeClass("error-msg");
        jQuery("#cause_state_error").html('');
        jQuery("#cause_category_error").html('');
        jQuery("#type_of_cause_error").html('');
        jQuery("#file1_error").html('');
        var file = jQuery('input[type="file"]').val();
        if (jQuery("#cause_organization_name").val() == '')
        {
            jQuery("#cause_organization_name").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#cause_contact_name").val() == '')
        {
            jQuery("#cause_contact_name").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#cause_contact_title").val() == '')
        {
            jQuery("#cause_contact_title").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#cause_street_address").val() == '')
        {
            jQuery("#cause_street_address").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#cause_city").val() == '')
        {
            jQuery("#cause_city").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#cause_zipcode").val() == '')
        {
            jQuery("#cause_zipcode").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#cause_state").val() == '')
        {
            jQuery("#cause_state_error").html('Select the name of the state')
            flag = 1;
        }
        if (jQuery("#cause_phone_no").val() == '')
        {
            jQuery("#cause_phone_no").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#cause_email_id").val() == '')
        {
            jQuery("#cause_email_id").addClass("error-msg");
            flag = 1;
        } else {
            if (!validateEmail(jQuery("#cause_email_id").val())) {
                jQuery("#cause_email_id").addClass("error-msg");
                flag = 1;
            }
        }
        if (jQuery("#cause_url").val() == '')
        {
            jQuery("#cause_url").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#cause_name").val() == '')
        {
            jQuery("#cause_name").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#end_cause_date").val() == '')
        {
            jQuery("#end_cause_date").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#end_cause_time").val() == '')
        {
            jQuery("#end_cause_time").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#end_cause_am_pm").val() == '')
        {
            jQuery("#end_cause_am_pm").addClass("error-msg");
            flag = 1;
        }

        if (jQuery("#category_of_the_cause").val() == '')
        {
            jQuery("#cause_category_error").html('Select the cause category');
            flag = 1;
        }
        if (jQuery("#enter_tags").val() == '')
        {
            jQuery("#enter_tags").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#additional_info").val() == '')
        {
            jQuery("#additional_info").addClass("error-msg");
            flag = 1;
        }
        if (jQuery('input[name=type_of_cause]:checked').length <= 0)
        {
            jQuery("#type_of_cause_error").html("Please select type of cause");
            flag = 1;
        }

        if (jQuery('input[name=type_of_cause]:checked').val() == 'Non-profit')
        {
            if (jQuery("#tax_id").val() == '')
            {
                jQuery("#tax_id").addClass("error-msg");
                flag = 1;
            }
        }
        if (jQuery("#fundraising_goal").val() == '')
        {
            jQuery("#fundraising_goal").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#file_cause").val() == '')
        {
            jQuery("#file1_error").html('Please select image');
            flag = 1;
        }

        if (flag == 1)
        {
            return false;
        } else {
            alert(flag);
            jQuery("#causeForm").submit();
            return true;
        }

    });


    /*for event form*/
    jQuery("#file_event").change(function () {
        if (typeof (FileReader) != "undefined") {
            var dvPreview = jQuery("#dvPreview1");
            dvPreview.html("");
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
            jQuery(jQuery(this)[0].files).each(function () {
                var file = jQuery(this);
                if (regex.test(file[0].name.toLowerCase())) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = jQuery("<img />");
                        img.attr("style", "height:100px;width: 150px");
                        img.attr("src", e.target.result);
                        dvPreview.append(img);
                    }
                    reader.readAsDataURL(file[0]);
                } else {
                    alert(file[0].name + " is not a valid image file.");
                    dvPreview.html("");
                    return false;
                }
            });
        } else {
            alert("This browser does not support HTML5 FileReader.");
        }
    });
    jQuery('#start_event_date').datepicker({
        dateFormat: 'yy-mm-dd'
    });
    jQuery('#end_event_date').datepicker({
        dateFormat: 'yy-mm-dd'
    });
    jQuery('#end_cause_date').datepicker({
        dateFormat: 'yy-mm-dd'
    });


    jQuery('.behavior_event_class').click(function () {
        jQuery("#no_volunteer").hide();
        jQuery("#fundraising_goal_event").hide();
        jQuery('input[name="behavior_event[]"]:checked').each(function () {
            if (jQuery(this).val() == 'Volunteer')
            {
                jQuery("#no_volunteer").show();
            } else if (jQuery(this).val() == 'Fundraiser')
            {
                jQuery("#fundraising_goal_event").show();
            }
        })
    })

    /*jQuery('#behavior_event_volunteer, #behavior_event_fund').click(function(event) {
     var $erMSG = $("#behavior_event_error");
     $(this).parent().parent().next().children('.form-control').toggle();
     !$('.behavior_event_class').is(':checked') && $erMSG.html("Please select type of cause") || $erMSG.html('');
     });*/

    jQuery('#event_submit').click(function (event) {
        var flag = 0;
        jQuery("#event_organization_name").removeClass("error-msg");
        jQuery("#event_contact_name").removeClass("error-msg");
        jQuery("#event_contact_title").removeClass("error-msg");
        jQuery("#event_street_address").removeClass("error-msg");
        jQuery("#event_city").removeClass("error-msg");
        jQuery("#event_state").removeClass("error-msg");
        jQuery("#event_state_error").html('');
        jQuery("#event_zipcode").removeClass("error-msg");
        jQuery("#event_phone_number").removeClass("error-msg");
        jQuery("#event_email_id").removeClass("error-msg");
        jQuery("#event_url").removeClass("error-msg");
        jQuery("#event_name").removeClass("error-msg");
        jQuery("#event_category_error").html('');
        jQuery("#enter_event_tags").removeClass("error-msg");
        jQuery("#start_event_date").removeClass("error-msg");
        jQuery("#start_event_time").removeClass("error-msg");
        jQuery("#start_event_am_pm").removeClass("error-msg");
        jQuery("#end_event_date").removeClass("error-msg");
        jQuery("#end_event_time").removeClass("error-msg");
        jQuery("#end_event_am_pm").removeClass("error-msg");
        jQuery("#event_address1").removeClass("error-msg");
        jQuery("#event_address2").removeClass("error-msg");
        jQuery("#event_address3").removeClass("error-msg");
        jQuery("#typeofevent_error").html('');
        jQuery("#event_tax_id").removeClass("error-msg");
        jQuery("#no_volunteer").removeClass("error-msg");
        jQuery("#fundraising_goal_event").removeClass("error-msg");
        jQuery("#file_event").html('');
        jQuery("#behavior_event_error").html('');


        var file = jQuery('input[type="file"]').val();

        if (jQuery("#event_organization_name").val() == '')
        {
            jQuery("#event_organization_name").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#event_contact_name").val() == '')
        {
            jQuery("#event_contact_name").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#event_contact_title").val() == '')
        {
            jQuery("#event_contact_title").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#event_street_address").val() == '')
        {
            jQuery("#event_street_address").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#event_city").val() == '')
        {
            jQuery("#event_city").addClass("error-msg");
            flag = 1;
        }

        if (jQuery("#event_state").val() == '')
        {
            jQuery("#event_state_error").html('Select the name of the state')
            flag = 1;
        }


        if (jQuery("#event_zipcode").val() == '')
        {
            jQuery("#event_zipcode").addClass("error-msg");
            flag = 1;
        }


        if (jQuery("#event_phone_number").val() == '')
        {
            jQuery("#event_phone_number").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#event_email_id").val() == '')
        {
            jQuery("#event_email_id").addClass("error-msg");
            flag = 1;
        } else {
            if (!validateEmail(jQuery("#event_email_id").val())) {
                jQuery("#event_email_id").addClass("error-msg");
                flag = 1;
            }
        }
        if (jQuery("#event_url").val() == '')
        {
            jQuery("#event_url").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#event_name").val() == '')
        {
            jQuery("#event_name").addClass("error-msg");
            flag = 1;
        }

        if (jQuery("#category_of_the_event").val() == '')
        {
            jQuery("#event_category_error").html('Select the cause category');
            flag = 1;
        }
        if (jQuery("#enter_event_tags").val() == '')
        {
            jQuery("#enter_event_tags").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#start_event_date").val() == '')
        {
            jQuery("#start_event_date").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#start_event_time").val() == '')
        {
            jQuery("#start_event_time").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#start_event_am_pm").val() == '')
        {
            jQuery("#start_event_am_pm").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#end_event_date").val() == '')
        {
            jQuery("#end_event_date").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#end_event_time").val() == '')
        {
            jQuery("#end_event_time").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#end_event_am_pm").val() == '')
        {
            jQuery("#end_event_am_pm").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#event_address1").val() == '')
        {
            jQuery("#event_address1").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#event_address2").val() == '')
        {
            jQuery("#event_address2").addClass("error-msg");
            flag = 1;
        }
        if (jQuery("#event_address3").val() == '')
        {
            jQuery("#event_address3").addClass("error-msg");
            flag = 1;
        }

        if (jQuery('input[name=typeofevent]:checked').length <= 0)
        {
            jQuery("#typeofevent_error").html("Please select type of cause");
            flag = 1;
        }

        if (jQuery('input[name=typeofevent]:checked').val() == 'Charity')
        {
            if (jQuery("#event_tax_id").val() == '')
            {
                jQuery("#event_tax_id").addClass("error-msg");
                flag = 1;
            }
        }

        if (!$('.behavior_event_class').is(':checked')) {
            jQuery("#behavior_event_error").html("Please select type of event");
            flag = 1;
        }


        if (jQuery('input[id=behavior_event_volunteer]:checked').val() == 'Volunteer')
        {
            if (jQuery("#no_volunteer").val() == '')
            {
                jQuery("#no_volunteer").addClass("error-msg");
                flag = 1;
            }
        }


        if (jQuery('input[id=behavior_event_fund]:checked').val() == 'Fundraiser')
        {
            if (jQuery("#fundraising_goal_event").val() == '')
            {
                jQuery("#fundraising_goal_event").addClass("error-msg");
                flag = 1;
            }
        }

        if (jQuery("#file_event").val() == '')
        {
            jQuery("#file_event_error").html('Please select image');
            flag = 1;
        }

        if (flag == 1)
        {
            return false;
        } else {
            /*alert(flag);*/
            jQuery("#eventForm").submit();
            return true;
        }

    });


    jQuery('input[name=typeofevent]').click(function () {
        jQuery("#typeofevent_error").html('');
        if (jQuery('input[name=typeofevent]:checked').length <= 0)
        {
            jQuery("#typeofevent_error").html("Please select type of event");
            flag = 1;
        } else {
            if (jQuery('input[name=typeofevent]:checked').val() == 'Charity')
            {
                jQuery("#event_tax_id").show();
            } else {
                jQuery("#event_tax_id").hide();
            }
        }
    });

});