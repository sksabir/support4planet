jQuery(document).ready(function () {
    jQuery('#share_email').keyup(function () {
        if (jQuery(this).val() == '') {
            jQuery(this).addClass('error-msg');
        } else {
            jQuery(this).removeClass('error-msg');
        }
    });
    jQuery("#send_email").on('click', function () {
        if (!validateEmail(jQuery('#share_email').val())) {
            jQuery('#share_email').addClass('error-msg');
            jQuery('#share_email').focus();
        } else {
            jQuery('#share_email').removeClass('error-msg');
            link = jQuery(this).attr('data-url');
            post_id = jQuery(this).attr('data-id');
            email_id = jQuery('#share_email').val();
            jQuery.get(link + '?email=' + email_id + '&post_id=' + post_id, function (data) {
                console.log(data);
                if (data != 'Error') {
                    msg = '<span style="color:#5f5">Link shared successfully.</span>';
                    jQuery('#share_link_box').html(msg);
                }
            });
        }
    });

    jQuery('#paypal-form').find('.form-control').keyup(function () {
        if (jQuery(this).val() == '') {
            jQuery(this).addClass('error-msg');
        } else {
            jQuery(this).removeClass('error-msg');
        }
    });

    jQuery("#not-logged-in").on('click', function () {
        $form = jQuery('#paypal-form');
        error = false;
        $form.find('.form-control').each(function () {
            if (jQuery(this).val() == '') {
                jQuery(this).addClass('error-msg');
                error = true;
            } else {
                jQuery(this).removeClass('error-msg');
            }
        });

        if (!error) {
            if (!validateEmail($form.find('.email').val())) {
                $form.find('.email').addClass("error-msg");
                $form.find('.email').focus();
                error = true;
            }
        }

        if (!error) {
            var url = $form.attr('action'); // the script where you handle the form input.

            jQuery.ajax({
                type: "POST",
                url: url,
                data: $form.serialize(), // serializes the form's elements.
                success: function (data)
                {
                    if (data != '0') {
                        jQuery('.item_number').val(data);
                        jQuery('#main-form').submit();
                        //alert(data);
                    }
                }
            });
        }
    });


    jQuery('#contact-causeForm .form-control').keyup(function () {
        if (jQuery(this).val() == '') {
            jQuery(this).addClass('error-msg');
        } else {
            jQuery(this).removeClass('error-msg');
        }
    });
    jQuery("#contact-causeForm").on('submit', function (e) {
        $form = jQuery(this);
        error = false;
        e.preventDefault();
        $form.find('.form-control').each(function () {
            if (jQuery(this).val() == '') {
                jQuery(this).addClass('error-msg');
                error = true;
            }
        });

        if (!error) {
            if (!validateEmail($form.find('.email_id').val())) {
                $form.find('.email_id').addClass("error-msg");
                $form.find('.email_id').focus();
                error = true;
            }
        }

        if (!error) {
            var url = $form.attr('action'); // the script where you handle the form input.

            jQuery.ajax({
                type: "POST",
                url: url,
                data: $form.serialize(), // serializes the form's elements.
                success: function (data)
                {
                    if (data != 'Error') {
                        $form[0].reset();
                        $form.find('#msg').html('Message sent successfully.');
                        setTimeout(function () {
                            $form.find('#msg').html('');
                        }, 5000);
                    }
                }
            });
            e.preventDefault();
        }
    });

    jQuery('#enter-amount').keyup(function () {
        var valid = /^\d{0,10}(\.\d{0,2})?$/.test(this.value),
                val = this.value;

        if (!valid) {
            console.log("Invalid input!");
            this.value = val.substring(0, val.length - 1);
        } else {
            jQuery('.paypal_amount').val(jQuery(this).val());
            jQuery(this).removeClass('error-msg');
        }
    });
});

function validateEmail(sEmail) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
        return true;
    } else {
        return false;
    }
}