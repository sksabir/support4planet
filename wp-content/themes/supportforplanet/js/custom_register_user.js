jQuery(document).ready(function () {
    jQuery('.registration-form .form-control').keyup(function () {
        if (jQuery(this).val() == '') {
            jQuery(this).addClass('error-msg');
        } else {
            jQuery(this).removeClass('error-msg');
        }
    });
    jQuery('.email_id').blur(function () {
        if (jQuery(this).val() != '') {
            if (validateEmail(jQuery(this).val())) {
                form_id = jQuery(this).attr('form-id');
                url = jQuery('#' + form_id).attr('varify-email'); // the script where you handle the form input.
                email = jQuery('#' + form_id).find('.email_id').val();

                var_code = randomPassword(10);
                str = var_code + ' ' + email + ' ' + url + ' ' + form_id;
                //alert(url+ ' '+email);
                jQuery.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        action: "check_email",
                        var_code: var_code,
                        email_id: email
                    },
                    success: function (data)
                    {
                        //alert(data); // show response from the php script.
                        if (data != 'Error') {
                            if (data != 'Email already Exist!') {
                                /*jQuery('#'+form_id).find('.var_code').html('<label class="col-sm-4 control-label">Varification Code:</label><div class="col-sm-8"><input class="form-control varification_code" name="varification_code" type="password" placeholder="Enter varification code"><span style="color:#0c0;" class="msg">A varification code is sent to your email.</span><button type="button" style="float:right; padding: 0px 10px;" onclick="resend(\''+str+'\');" class="resend_var">Resend Code</button></div>');
                                 jQuery('#'+form_id).find('.var_code').show();*/
                                jQuery('#' + form_id).find('.var_code').html('');
                                jQuery('#' + form_id).find('.var_code').hide();
                                jQuery('.email_id').removeClass("error-msg");
                            } else {
                                jQuery('#' + form_id).find('.var_code').html('<label class="col-sm-4 control-label"></label><div class="col-sm-8"><span style="color:#c00;" class="msg er_msg">Email already Exist!</span></div>');
                                jQuery('#' + form_id).find('.var_code').show();
                                jQuery('.email_id').addClass("error-msg");
                            }
                        } else {
                            jQuery('#' + form_id).find('.var_code').html('<label class="col-sm-4 control-label"></label><div class="col-sm-8"><span style="color:#c00;" class="msg er_msg">An Error Occured!</span></div>');
                            jQuery('#' + form_id).find('.var_code').show();
                        }
                    }
                });
            } else {
                jQuery(this).addClass('error-msg');
            }
        }
    });
    jQuery(".registration-form").on('submit', function (e) {
        $form = jQuery(this);
        error = false;
        email_exists = false;
        e.preventDefault();
        $form.find('.form-control').each(function () {
            if (jQuery(this).val() == '') {
                jQuery(this).addClass('error-msg');
                error = true;
            }
        });

        if (!error) {
            if (!validateEmail($form.find('.email_id').val())) {
                $form.find('.email_id').addClass("error-msg");
                $form.find('.email_id').focus();
                error = true;
            }
        }

        $form.find('.er_msg').each(function () {
            email_exists = true;
        });

        if (!error && email_exists) {
            $form.find('.email_id').addClass("error-msg");
            $form.find('.email_id').focus();
            error = true;
        }
        if (!error) {
            if ($form.find('.pass').val() != $form.find('.pass-conf').val())
            {
                $form.find('.pass-conf').val('');
                $form.find('.pass-conf').addClass('error-msg');
                $form.find('.pass-conf').focus();
                error = true;
            }
        }
        /*if(!error && !email_varify){
         url = $form.attr('varify-email'); // the script where you handle the form input.
         email = $form.find('.email_id').val();
         form_id = $form.attr("id");
         var_code = randomPassword(10);
         str = var_code + ' ' + email + ' ' + url + ' ' + form_id;
         //alert(url+ ' '+email);
         jQuery.ajax({
         type: "POST",
         url: url,
         data: {
         action: "varify_email",
         var_code: var_code,
         email_id: email
         },
         success: function(data)
         {
         //alert(data); // show response from the php script.
         $form.find('.var_code').html('<label class="col-sm-4 control-label">Varification Code:</label><div class="col-sm-8"><input class="form-control varification_code" name="varification_code" type="password" placeholder="Enter varification code"><span style="color:#0c0;" class="msg">A varification code is sent to your email.</span><button type="button" style="float:right; padding: 0px 10px;" onclick="resend(\''+str+'\');" class="resend_var">Resend Code</button></div>');
         $form.find('.var_code').show();
         }
         });
         
         //jQuery(this).unbind('submit').submit();
         }*/
        if (!error /*&& email_varify*/) {

            //alert(var_code + ' ' + $form.find('.varification_code').val())
            //if(var_code==$form.find('.varification_code').val()){
            jQuery(this).unbind('submit').submit();
            /*}
             else{
             $form.find('.var_code').html('<label class="col-sm-4 control-label">Varification Code:</label><div class="col-sm-8"><input class="form-control varification_code" name="varification_code" type="password" placeholder="Enter varification code"><span style="color:#c00;" class="msg">Wrong varification code!</span><button type="button" style="float:right; padding: 0px 10px;" onclick="resend(\''+str+'\');" class="resend_var">Resend Code</button></div>');
             $form.find('.var_code').show();
             }*/
        }
    });
});

function validateEmail(sEmail) {
    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
    if (filter.test(sEmail)) {
        return true;
    } else {
        return false;
    }
}
function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}
function resend(str) {
    alert(str);
    var res = str.split(" ");
    //alert(url+ ' '+email);

    jQuery.ajax({
        type: "POST",
        url: res[2],
        data: {
            action: "varify_email",
            var_code: res[0],
            email_id: res[1]
        },
        success: function (data)
        {
            //alert(data); // show response from the php script.
            if (data != 'Error') {
                if (data != 'Email already Exist!') {
                    jQuery('#' + form_id).find('.var_code').html('<label class="col-sm-4 control-label">Varification Code:</label><div class="col-sm-8"><input class="form-control varification_code" name="varification_code" type="password" placeholder="Enter varification code"><span style="color:#0c0;" class="msg">A varification code is sent to your email.</span><button type="button" style="float:right; padding: 0px 10px;" onclick="resend(\'' + str + '\');" class="resend_var">Resend Code</button></div>');
                    jQuery('#' + form_id).find('.var_code').show();
                } else {
                    jQuery('#' + form_id).find('.var_code').html('<label class="col-sm-4 control-label"></label><div class="col-sm-8"><span style="color:#c00;" class="msg">Email already Exist!</span></div>');
                    jQuery('#' + form_id).find('.var_code').show();
                }
            } else {
                jQuery('#' + form_id).find('.var_code').html('<label class="col-sm-4 control-label"></label><div class="col-sm-8"><span style="color:#c00;" class="msg">An Error Occured!</span></div>');
                jQuery('#' + form_id).find('.var_code').show();
            }
        }
    });
}