<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <?php if (is_singular() && pings_open(get_queried_object())) : ?>
            <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php endif; ?>
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.theme.default.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/easy-responsive-tabs.css"/>
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <!-- header -->
        <header>
            <div class="container-fluid header-top">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3"><?php show_easylogo(); ?></div>
                        <div class="col-sm-9 right-login-section">
                            <div class="col-md-12 no-padding">
                                <div class="user_login">
                                    <?php
                                    if (is_user_logged_in()) {
                                        $user_ID = get_current_user_id();
                                        $pic = get_field('profile_picture', 'user_' . $user_ID);
                                        if (!empty($pic)) {
                                            $profile_picture = get_field('profile_picture', 'user_' . $user_ID);
                                            $url = $profile_picture['url'];
                                            $alt = $profile_picture['alt'];
                                        } else {
                                            $url = get_template_directory_uri() . '/images/blank.jpg';
                                            $alt = '';
                                        }
                                        ?>
                                        <ul>
                                            <li><a href="#"  class="notification get-it" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo get_template_directory_uri(); ?>/images/bell.png" alt=""> <span>3</span> </a>
                                                <ul class="dropdown-menu notificatioDropdown" aria-labelledby="dLabel">
                                                    <li>
                                                        <a href="#">
                                                            <div class="profileThumb"><img src="<?php echo get_template_directory_uri(); ?>/images/profilePic.png" alt=""></div>
                                                            <div class="notificationdetail">
                                                                <p>Big Cat Rescue has thanked you for donation</p>
                                                                <p><img src="<?php echo get_template_directory_uri(); ?>/images/smily.png" alt=""><span>1 hr ago</span></p>
                                                            </div>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#">
                                                            <div class="profileThumb"><img src="<?php echo get_template_directory_uri(); ?>/images/profilePic.png" alt=""></div>
                                                            <div class="notificationdetail">
                                                                <p>Austin Pets has commented on
                                                                    Big Cat Rescue’s post</p>
                                                                <p><img src="<?php echo get_template_directory_uri(); ?>/images/comment.png" alt=""><span>1 hr ago</span></p>
                                                            </div>
                                                            <div class="notificationPost"><img src="<?php echo get_template_directory_uri(); ?>/images/notificationposts.jpg" alt=""></div>
                                                        </a>
                                                    </li>

                                                    <li>
                                                        <a href="#">
                                                            <div class="profileThumb"><img src="<?php echo get_template_directory_uri(); ?>/images/profilePic.png" alt=""></div>
                                                            <div class="notificationdetail">
                                                                <p>Big Cat Rescue has thanked you for donation</p>
                                                                <p><img src="<?php echo get_template_directory_uri(); ?>/images/smily.png" alt=""><span>1 hr ago</span></p>
                                                            </div>
                                                        </a>
                                                    </li>

                                                    <li><a href="#" class="more">See all Notifications</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="#" class="profileMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> 
                                                    <span class="profilePic">
                                                        <img src="<?php echo $url; ?>" alt="<?php echo $alt; ?>" />
                                                    </span> 
                                                    <span class="dropdownInfo"></span>
                                                </a>

                                                <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li><a href="#">Donation Summary</a></li>
                                                    <li><a href="<?php echo site_url(); ?>/edit-profile/">Edit Profile</a></li>
                                                    <li><a href="#">Account Settings</a></li>
                                                    <li><a href="<?php echo wp_logout_url(home_url()); ?>">Logout</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <?php
                                    } else {
                                        ?>
                                        <ul>
                                            <li><a href="<?php echo site_url(); ?>/log-in/" >Login </a></li>
                                            <li><a href="<?php echo site_url(); ?>/create-account/"> Create Account</a></li>
                                        </ul>
                                        <button type="button" class="btn btn-primary"> Donate</button>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-12 no-padding">
                                <div class="col-sm-5 no-padding">
                                    <form role="search" method="get" id="searchform"
                                          class="searchform" action="<?php echo esc_url(home_url('/')); ?>">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12 col-xs-12">
                                                <div class="input-group">
                                                    <!-- <label class="screen-reader-text" for="s"><?php _x('Search for:', 'label'); ?></label> -->
                                                    <form role="search" method="get" id="searchform" action="<?php bloginfo('url') . "/" ?>" >
                                                        <input type="text" value="<?php
                                                        if (!empty($_GET['s'])) {
                                                            echo $_GET['s'];
                                                        }
                                                        ?>" name="s" id="s" class="form-control input-search" placeholder="Search Cause"/>
                                                        <span class="input-group-btn">
                                                            <button id="searchsubmit" class="btn btn-primary" type="submit">
                                                                <i class="fa fa-search"></i>
                                                            </button>
                                                        </span>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-sm-7 planetnav no-padding">
                                    <nav class="navbar navbar-default"> 
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                                        </div>

                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <!--<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">-->
                                        <!-- <ul class="nav navbar-nav">
                                          <li class="active"><a href="index.html">Home</a></li>
                                          <li><a href="about.html"> About Us </a></li>
                                          <li><a href="cause.html">Causes </a></li>
                                          <li><a href="start_cause.html"> Start A Cause</a></li>
                                          <li><a href="contact.html"> Contact Us</a></li>
                                        </ul> -->
                                        <?php
                                        wp_nav_menu(array(
                                            'theme_location' => 'primary',
                                            'container_class' => 'collapse navbar-collapse',
                                            'container_id' => 'bs-example-navbar-collapse-1',
                                            'menu_class' => 'nav navbar-nav'
                                        ));
                                        ?>
                                        <!--</div>-->
                                        <!-- /.navbar-collapse --> 
                                        <!-- /.container-fluid --> 
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </header>
    <!-- header end --> 