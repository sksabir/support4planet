<?php

add_action("wp_ajax_all_pagination", "all_pagination_fn");
add_action("wp_ajax_nopriv_all_pagination", "all_pagination_fn");

function all_pagination_fn() {
    $page = $_POST['page'];
    $type = $_POST['type'];
    $flag = 0;
    if ($type == '') {
        $args = array(
            'posts_per_page' => 3,
            'offset' => 3 * $page,
            'category' => '',
            'category_name' => '',
            'orderby' => 'date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' => '',
            'post_type' => array('event', 'cause'),
            'post_mime_type' => '',
            'post_parent' => '',
            'author' => '',
            'post_status' => 'publish',
            'suppress_filters' => true
        );

        $totalargs = array(
            'posts_per_page' => -1,
            'category' => '',
            'category_name' => '',
            'orderby' => 'date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' => '',
            'post_type' => array('event', 'cause'),
            'post_mime_type' => '',
            'post_parent' => '',
            'author' => '',
            'post_status' => 'publish',
            'suppress_filters' => true
        );
        $total_array = get_posts($totalargs);
        $count = count($total_array);

        if ($count > (3 * ($page + 1))) {
            $flag = 1;
        }
        //echo $flag;
    } else {
        $args = array(
            'posts_per_page' => 3,
            'offset' => 3 * $page,
            'orderby' => 'date',
            'order' => 'DESC',
            'post_type' => array('event', 'cause'),
            'post_status' => 'publish',
            'suppress_filters' => true,
            'tax_query' => array(
                array(
                    'taxonomy' => 'cause_category',
                    'field' => 'id',
                    'terms' => array($type)
                )
            ),
        );
        $totalargs = array(
            'posts_per_page' => -1,
            'orderby' => 'date',
            'order' => 'DESC',
            'post_type' => array('event', 'cause'),
            'post_status' => 'publish',
            'suppress_filters' => true,
            'tax_query' => array(
                array(
                    'taxonomy' => 'cause_category',
                    'field' => 'id',
                    'terms' => array($type)
                )
            ),
        );
        $total_array = get_posts($totalargs);
        $count = count($total_array);
        if ($count > 3 * ($page + 1)) {
            $flag = 1;
        }
    }

    $all_array = get_posts($args);
    $clases = '';
    $html = '';
    if (count($all_array) > 0) {
        foreach ($all_array as $all) {
            $term_list = wp_get_post_terms($all->ID, 'cause_category', array("fields" => "slugs"));
            if (count($term_list) > 0) {
                $clases = implode(" ", $term_list);
            }
            if (get_the_post_thumbnail($all->ID, 'thumbnail') == "") {

                $img = '<img src="' . site_url() . '/wp-content/uploads/2016/04/placeholder.png" alt=""/>';
            } else {
                $img = get_the_post_thumbnail($all->ID, 'thumbnail');
            }
            $content = '';
            if (strlen($all->post_content) > 100) {
                $content = substr(strip_tags($all->post_content), 0, 100) . "...";
            } else {
                $content = substr(strip_tags($all->post_content), 0, 100);
            }
            $html.='<div style=" display: inline-block; opacity: 1;" class="portfolio all mix_all ' . $clases . ' latest" data-cat="logo">';
            $html.='<div class="featured-causes-box"><a href="">' . $img . '</a>';
            $html.='<div class="shear"><a href="">like</a></div>';
            $html.='<div class="docoment-section">';
            $html.='<h2><a href="">' . $all->post_title . '</a></h2>';
            $html.='<p>' . $content . '</p>';
            $html.='<span class="graph"> <span class="pull-right">15%</span>';
            $html.='<div class="progress progress-warning active">';
            $html.='<div style="width: 15%;" class="bar"></div>';
            $html.='</div>';
            $html.='<strong>12 Days Left</strong> </span> </div>';
            $html.='<div class="grey-support">';
            $html.='<ul>';
            $html.='<li><span class="rupee"></span>';
            $html.='<p>Funded 80%</p>';
            $html.=' </li>';
            $html.='<li><span class="doller"></span>';
            $html.='<p>Pledged $2453</p>';
            $html.='</li>';
            $html.='</ul>';
            $html.='<div class="supprot"><a href="">Support</a></div>';
            $html.='</div>';
            $html.=' </div>';
            $html.=' </div>';
        }
        if ($flag == 1) {
            $html.='<div class="all-view-causes"><a  class="view-causes all-pagi" id="all-pagi" page="1" type="">View More Causes</a> </div>';
        }
        echo $html;
        exit;
    } else {
        $html = "";
        exit;
    }
}

add_action("wp_ajax_cat_search", "cat_search_fn");
add_action("wp_ajax_nopriv_cat_search", "cat_search_fn");

function cat_search_fn() {
    $cat = $_POST['cat'];
    $args = array(
        'posts_per_page' => 3,
        'orderby' => 'date',
        'order' => 'DESC',
        'post_type' => array('event', 'cause'),
        'post_status' => 'publish',
        'suppress_filters' => true,
        'tax_query' => array(
            array(
                'taxonomy' => 'cause_category',
                'field' => 'id',
                'terms' => array($cat)
            )
        ),
    );
    $all_array = get_posts($args);
    $clases = '';
    $html = '';
    if (count($all_array) > 0) {
        //$html.=' <div id="portfoliolist">';
        foreach ($all_array as $all) {
            $term_list = wp_get_post_terms($all->ID, 'cause_category', array("fields" => "slugs"));
            if (count($term_list) > 0) {
                $clases = implode(" ", $term_list);
            }
            if (get_the_post_thumbnail($all->ID, 'thumbnail') == "") {

                $img = '<img src="' . site_url() . '/wp-content/uploads/2016/04/placeholder.png" alt=""/>';
            } else {
                $img = get_the_post_thumbnail($all->ID, 'thumbnail');
            }
            $content = '';
            if (strlen($all->post_content) > 100) {
                $content = substr(strip_tags($all->post_content), 0, 100) . "...";
            } else {
                $content = substr(strip_tags($all->post_content), 0, 100);
            }
            $html.='<div style=" display: inline-block; opacity: 1;" class="portfolio all mix_all ' . $clases . ' latest" data-cat="logo">';
            $html.='<div class="featured-causes-box"><a href="">' . $img . '</a>';
            $html.='<div class="shear"><a href="">like</a></div>';
            $html.='<div class="docoment-section">';
            $html.='<h2><a href="">' . $all->post_title . '</a></h2>';
            $html.='<p>' . $content . '</p>';
            $html.='<span class="graph"> <span class="pull-right">15%</span>';
            $html.='<div class="progress progress-warning active">';
            $html.='<div style="width: 15%;" class="bar"></div>';
            $html.='</div>';
            $html.='<strong>12 Days Left</strong> </span> </div>';
            $html.='<div class="grey-support">';
            $html.='<ul>';
            $html.='<li><span class="rupee"></span>';
            $html.='<p>Funded 80%</p>';
            $html.=' </li>';
            $html.='<li><span class="doller"></span>';
            $html.='<p>Pledged $2453</p>';
            $html.='</li>';
            $html.='</ul>';
            $html.='<div class="supprot"><a href="">Support</a></div>';
            $html.='</div>';
            $html.=' </div>';
            $html.=' </div>';
        }
        // $html.='</div> <div class="all-view-causes"><a  class="view-causes all-pagi" id="all-pagi" page="1" type="'.$cat.'">View More Causes</a> </div>';
        echo $html;
        exit;
    } else {
        $html = "";
        exit;
    }
}

add_action("wp_ajax_zipcode_search", "zipcode_search_fn");
add_action("wp_ajax_nopriv_zipcode_search", "zipcode_search_fn");

function zipcode_search_fn() {
    $zipcode = $_POST['zipcode'];
    $args = array(
        'posts_per_page' => 10,
        'orderby' => 'date',
        'order' => 'DESC',
        'post_type' => array('event', 'cause'),
        'post_status' => 'publish',
        'suppress_filters' => true,
        'meta_query' => array(
            array(
                'key' => 'zipcode',
                'value' => $zipcode,
                'compare' => '=',
            )
        )
    );
    $all_array = get_posts($args);
    $clases = '';
    $html = '';
    if (count($all_array) > 0) {
        foreach ($all_array as $all) {
            $term_list = wp_get_post_terms($all->ID, 'cause_category', array("fields" => "slugs"));
            if (count($term_list) > 0) {
                $clases = implode(" ", $term_list);
            }
            $html.='<div style=" display: inline-block; opacity: 1;" class="portfolio all mix_all ' . $clases . ' latest" data-cat="logo">';
            $html.='<div class="featured-causes-box"><a href="">' . get_the_post_thumbnail($all->ID, "thumbnail") . '</a>';
            $html.='<div class="shear"><a href="">like</a></div>';
            $html.='<div class="docoment-section">';
            $html.='<h2><a href="">' . $all->post_title . '</a></h2>';
            $html.='<p>Supporting charities our community believes in by helping them reach and exceed their fundraising goals with the hope of making the planet a better place for all. </p>';
            $html.='<span class="graph"> <span class="pull-right">15%</span>';
            $html.='<div class="progress progress-warning active">';
            $html.='<div style="width: 15%;" class="bar"></div>';
            $html.='</div>';
            $html.='<strong>12 Days Left</strong> </span> </div>';
            $html.='<div class="grey-support">';
            $html.='<ul>';
            $html.='<li><span class="rupee"></span>';
            $html.='<p>Funded 80%</p>';
            $html.=' </li>';
            $html.='<li><span class="doller"></span>';
            $html.='<p>Pledged $2453</p>';
            $html.='</li>';
            $html.='</ul>';
            $html.='<div class="supprot"><a href="">Support</a></div>';
            $html.='</div>';
            $html.=' </div>';
            $html.=' </div>';
        }
        echo $html;
        exit;
    } else {
        $html = "";
        exit;
    }
}

?>