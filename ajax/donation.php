<?php
require("../wp-load.php");
global $wpdb;

//For logged in users start
if($_POST['email']=='' && $_POST['user_id']!='')
{
	$user_id 	= trim($_POST['user_id']);
	$post_id 	= trim($_POST['post_id']);
	$amount 	= trim($_POST['amount']);
	$donation_sql  = "INSERT INTO {$wpdb->prefix}donation SET `post_id`='".$post_id."',`user_id`='".$user_id."',`amount`='".$amount."',`date`='".date('Ymd')."',`status`='unpaid'";
	$query_run = $wpdb->query($donation_sql, OBJECT);
	if($query_run)
	{
		echo  $wpdb->insert_id;				
	}
	else
	{
		echo '0';
	}
}
//For logged in users end

//For not logged in users start
if($_POST['email']!='' && $_POST['user_id']=='')
{
	$email 	= trim($_POST['email']);
	$post_id = trim($_POST['post_id']);
	$amount = trim($_POST['amount']);

	//For not logged in users but registered with email start
	if ( email_exists($email) ) {
		$user_id = email_exists($email);

		$donation_sql  = "INSERT INTO {$wpdb->prefix}donation SET `post_id`='".$post_id."',`user_id`='".$user_id."',`amount`='".$amount."',`date`='".date('Ymd')."',`status`='unpaid'";
		$query_run = $wpdb->query($donation_sql, OBJECT);
		if($query_run)
		{
			echo  $wpdb->insert_id;				
		}
		else
		{
			echo '0';
		}
	}
	//For not logged in users but registered with email end

	//For not logged in users and not registered with email start
	else {
		
		$pass = randomPassword();
		$login_link = site_url().'/log-in/';
		$userdata = array(
            'user_login' => $email,
            'user_pass' => $pass,  // When creating an user, `user_pass` is expected.
            'user_email' => $email,
            'role' => 'individual'
        );
        $user_ID = wp_insert_user( $userdata );
        update_field('field_56de7e139d862', $_POST['night_phone_a'], 'user_'.$user_ID);

        update_user_meta($user_ID, 'first_name', $_POST['first_name']);
        update_user_meta($user_ID, 'last_name', $_POST['last_name']);
        update_user_meta( $user_ID, 'raw_password', $pass);

		update_field('field_56efa9bb2ed06', $_POST['address_1'], 'user_'.$user_ID);
		update_field('field_56efa9d92ed07', $_POST['city'], 'user_'.$user_ID);
		update_field('field_56efa9e52ed08', $_POST['state'], 'user_'.$user_ID);
		update_field('field_56efa9f42ed09', $_POST['zip'], 'user_'.$user_ID);


        $to = $email;
		$subject = "Thank You - Support for Planet";

		$message = "
		<html>
		<head>
		<title>Thank You - Support for Planet</title>
		</head>
		<body>
		<p>Thank You for your interest.</br>Your account is successfully created and here are the details:</p>
		<table>
		<tr>
		<td><b>Username: </b></td>
		<td>$email</td>
		</tr>
		<tr>
		<td><b>Password: </b></td>
		<td>$pass</td>
		</tr>
		</table>
		<p>You can login from <a href='$login_link'>here</a></p>
		<p>Thank You.</p>
		</body>
		</html>
		";

		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: Support for Planet<admin@supportforplanet.com>' . "\r\n";

		$report = mail($to,$subject,$message,$headers);

		$donation_sql  = "INSERT INTO {$wpdb->prefix}donation SET `post_id`='".$post_id."',`user_id`='".$user_ID."',`amount`='".$amount."',`date`='".date('Ymd')."',`status`='unpaid'";
		$query_run = $wpdb->query($donation_sql, OBJECT);
		if($query_run)
		{
			echo  $wpdb->insert_id;				
		}
		else
		{
			echo '0';
		}
	}
	//For not logged in users and not registered with email end
}



function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}